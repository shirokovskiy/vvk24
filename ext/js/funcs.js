/**
 * Здесь все функции для фронт и бэк-ендов данного сайта
 * Все функции общего пользования в gui.js
 */

/* GA */
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-45661714-1', 'vvk24.ru');
ga('send', 'pageview');


/**
 * For the JCarousel
 * @param carousel
 */
function mycarousel_initCallback(carousel)
{
	// Disable autoscrolling if the user clicks the prev or next button.
	carousel.buttonNext.bind('click', function() {
		carousel.startAuto(0);
	});

	carousel.buttonPrev.bind('click', function() {
		carousel.startAuto(0);
	});
}

/**
 * Установить тип поиска (вакансии или резюме)
 * @param type
 */
function setSearchForm(type) {
	if(!isEmpty(type)) {
		$('#type').attr('value', type);
	}
}

$(function(){
	$.datepicker.setDefaults( $.datepicker.regional[ "ru" ] );

	if ($('a.uc')) // Under Construction
		$('a.uc').fancybox();
	if ($('a.fb')) // FancyBoxed Layer
		$('a.fb').fancybox();

    /**
     * Top-banners in Carousel
     */
	$('#top_banner').jcarousel({
		auto: 4,
        scroll: 1,
		wrap: 'last',
		initCallback: mycarousel_initCallback
	});


    /**
     * Блок крутящихся логотипов
     */
    /*if( $('#brandsCanvas') ) {
        if( ! $('#brandsCanvas').tagcanvas({
            shape : "vring",
            lock : "y",
            stretchX : 11,
            outlineThickness : 2,
            outlineColour : '#757575',
            maxSpeed : 0.15,
            depth : 0.75
        }, 'company_tags')) {
            // TagCanvas failed to load
            $('#tag-icons-cloud').hide();
        }
    }*/
});

