<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Вакансия [vacancy] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "vacancy.50.tpl");

define('NOINFO', '<i class="light_grey">нет информации</i>');

/**
 * Если указан параметр записи
 */
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    $intRecordId = intval($arrReqUri[1]);

    if ($intRecordId > 0) {
        //
        $strSqlQuery = "SELECT * FROM `job_vacancies`"
            ." LEFT JOIN `job_companies` ON (jc_id = jv_jc_id)"
            ." WHERE `jv_id` = ".$intRecordId;
        $arrRecord = $objDb->fetch($strSqlQuery);

        if (is_array($arrRecord) && !empty($arrRecord)) {
            $arrTplVars['intVacancyId'] = $intRecordId;
            $arr_string_fields = array('strTitle'=>'jv_title', 'strCompany'=>'jc_title', 'strFio'=>'jv_contact_fio', 'strEmail'=>'jv_email', 'strPhone'=>'jv_phone', 'strCity'=>'jv_city', 'strDescription'=>'jv_description');

            foreach($arr_string_fields as $key => $field) {
                $arrRecord[$key] = (!empty($arrRecord[$field]) ? htmlspecialchars($arrRecord[$field]) : NOINFO);
                $arrIf['is.'.$key] = $arrRecord[$key] != NOINFO;
            }
            $arrRecord['strTitle'] = mb_ucfirst($arrRecord['strTitle']);
            $arrTplVars['m_title'] = $arrRecord['strTitle'];
            if (!empty($arrRecord['strDescription'])) $arrRecord['strDescription'] = nl2br($arrRecord['strDescription']);

            if(!empty($arrRecord['strCompany']) && intval($arrRecord['jc_id'])) {
                $arrRecord['strCompany'] = '<a href="'.SITE_URL.'company-vacancies/'.$arrRecord['jc_id'].'">'.$arrRecord['strCompany'].'</a>';
            }

            $arrRecord['strDriver'] = $arrRecord['jv_driver'] == 'Y' ? 'требуется' : NOINFO;
            $arrIf['is.strDriver'] = $arrRecord['strDriver'] != NOINFO;

            $arrRecord['strTrips'] = $arrRecord['jv_trips'] == 'Y' ? 'требуется' : NOINFO;
            $arrIf['is.strTrips'] = $arrRecord['strTrips'] != NOINFO;

            $arrRecord['strWorkbusy'] = $arrRecord['strEdu'] = NOINFO;
            if(intval($arrRecord['jv_jw_id'])) {
                include_once "models/JobWorkbusy.php";
                $objWB = new JobWorkbusy($_db_config);
                $arrRecord['strWorkbusy'] = $objWB->getStatus($arrRecord['jv_jw_id']);
            }
            if(intval($arrRecord['jv_jet_id'])) {
                include_once "models/JobEduType.php";
                $objET = new JobEduType($_db_config);
                $arrRecord['strEdu'] = $objET->getStatus($arrRecord['jv_jet_id']);
            }
            $arrIf['is.strEdu'] = $arrRecord['strEdu'] != NOINFO;

            $arrRecord['strSex'] = $arrRecord['jv_sex'] == 'male' ? 'мужчина' : ($arrRecord['jv_sex'] == 'female' ? 'женщина' : NOINFO);
            $arrIf['is.strSex'] = $arrRecord['strSex'] != NOINFO;

            $arrIf['is.strSalary'] = true;
            $arrRecord['strSalary'] = (!empty($arrRecord['jv_salary_from']) ? 'от '.$arrRecord['jv_salary_from'] : '').(!empty($arrRecord['jv_salary_to']) ? (!empty($arrRecord['jv_salary_from'])?' до ':'').$arrRecord['jv_salary_to'] : '');
            if (!empty($arrRecord['strSalary']) && $arrRecord['strSalary'] != NOINFO) {
                $arrRecord['strSalary'] .= ' руб';
            } elseif (!empty($arrRecord['jv_salary'])) {
                $arrRecord['strSalary'] = $arrRecord['jv_salary'];
            } else {
                $arrRecord['strSalary'] = NOINFO;
                $arrIf['is.strSalary'] = false;
            }

            if ($arrRecord['strPhone']!=NOINFO && strlen($arrRecord['strPhone'])==10 && !preg_match('/\s/i', $arrRecord['strPhone'])) {
                $arrRecord['strPhone'] = '+7 '.substr($arrRecord['strPhone'],0,3).' '.substr($arrRecord['strPhone'],3,3).' '.substr($arrRecord['strPhone'],6,2).' '.substr($arrRecord['strPhone'],8,2);
            }

            if ( date('Y', $arrRecord['jv_date_publish']) > 2012 ) {
                $arrRecord['strDatePublish'] = date("d.m.Y", $arrRecord['jv_date_publish']);
            }

            $objTpl->tpl_array("page.content", $arrRecord);
        }
    }
}

$objTpl->tpl_array("page.content", $arrTplVars);
$objTpl->tpl_if("page.content", $arrIf);
