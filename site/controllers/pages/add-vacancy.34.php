<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Добавить вакансию [add-vacancy] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "add-vacancy.34.tpl");

include_once "models/Company.php";
$objCompany = new Company($_db_config);

include_once "models/Vacancy.php";
$objVacancy = new Vacancy($_db_config);

$intStatusError=0;

if(isset($_POST) && $_POST['add'] == 'vacancy') {
    $arrSqlData['strCity'] = mysql_real_escape_string(trim($_POST['strCity']));
    $arrTplVars['strCity'] = htmlspecialchars(stripslashes(trim($_POST['strCity'])));

    if (empty($arrSqlData['strCity']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Город';
    }

    $arrSqlData['strTitle'] = mysql_real_escape_string(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Должность';
    }

    $arrSqlData['strSalaryFrom'] = mysql_real_escape_string(trim($_POST['strSalaryFrom']));
    $arrTplVars['strSalaryFrom'] = htmlspecialchars(stripslashes(trim($_POST['strSalaryFrom'])));

    $arrSqlData['strSalaryTo'] = mysql_real_escape_string(trim($_POST['strSalaryTo']));
    $arrTplVars['strSalaryTo'] = htmlspecialchars(stripslashes(trim($_POST['strSalaryTo'])));

    $arrSqlData['strAgeFrom'] = mysql_real_escape_string(trim($_POST['strAgeFrom']));
    $arrTplVars['strAgeFrom'] = htmlspecialchars(stripslashes(trim($_POST['strAgeFrom'])));

    $arrSqlData['strAgeTo'] = mysql_real_escape_string(trim($_POST['strAgeTo']));
    $arrTplVars['strAgeTo'] = htmlspecialchars(stripslashes(trim($_POST['strAgeTo'])));

    $rbSex = $_POST['rbSex']?:'N';
    $arrTplVars['jv_sex_'.$rbSex] = " checked";

    //$rbEdu = $_POST['rbEdu']?:'N';
    //$sbxWorkbusy = $_POST['sbxWorkbusy'];

    //$rbAgency = $_POST['rbAgency']?:'N';
    //$arrTplVars['rbAgency_'.$rbAgency] = ' checked';

//    $cbxDriver = $_POST['cbxDriver']?:'N';
//    $arrTplVars['cbxDriver'.$cbxDriver] = ' checked';

//    $cbxTrips = $_POST['cbxTrips']?:'N';
//    $arrTplVars['cbxTrips'.$cbxTrips] = ' checked';

    $arrSqlData['strExp'] = mysql_real_escape_string(trim($_POST['strExp']));
    $arrTplVars['strExp'] = htmlspecialchars(stripslashes(trim($_POST['strExp'])));

    $arrSqlData['strDescription'] = mysql_real_escape_string(trim($_POST['strDescription']));
    $arrTplVars['strDescription'] = htmlspecialchars(stripslashes(trim($_POST['strDescription'])));

    $arrSqlData['strContactPerson'] = mysql_real_escape_string(trim($_POST['strContactPerson']));
    $arrTplVars['strContactPerson'] = htmlspecialchars(stripslashes(trim($_POST['strContactPerson'])));

//    $arrSqlData['strPhone'] = mysql_real_escape_string(trim($_POST['strPhone']));
//    $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));

//    if (empty($arrSqlData['strPhone']) ) {
//        $intStatusError=1;
//        $arrCodeError[] = 'Вы не заполнили Телефон';
//    }

    $arrSqlData['strEmail'] = mysql_real_escape_string(trim($_POST['strEmail']));
    $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

    /*if (empty($arrSqlData['strEmail']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Email';
    }*/

    $arrSqlData['strCompany'] = mysql_real_escape_string(trim($_POST['strCompany']));
    $arrTplVars['strCompany'] = htmlspecialchars(stripslashes(trim($_POST['strCompany'])));

    if (empty($arrSqlData['strCompany']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили Наименование Компании';
    }

//    $arrSqlData['strCompanyCity'] = mysql_real_escape_string(trim($_POST['strCompanyCity']));
//    $arrTplVars['strCompanyCity'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyCity'])));

    $arrSqlData['strAddress'] = mysql_real_escape_string(trim($_POST['strAddress']));
    $arrTplVars['strAddress'] = htmlspecialchars(stripslashes(trim($_POST['strAddress'])));

//    $arrSqlData['strWeb'] = mysql_real_escape_string(trim($_POST['strWeb']));
//    $arrTplVars['strWeb'] = htmlspecialchars(stripslashes(trim($_POST['strWeb'])));

    $arrSqlData['strCompanyEmail'] = mysql_real_escape_string(trim($_POST['strCompanyEmail']));
    $arrTplVars['strCompanyEmail'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyEmail'])));

    $arrPhone = $_POST['phone'];
    if (is_array($arrPhone) && !empty($arrPhone)) {
        if (isset($arrPhone[2])) $arrPhone[2] = '*'.$arrPhone[2];
        $_POST['strCompanyPhone'] = implode(' ', $arrPhone);
    } else {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили Телефон';
    }

    $arrSqlData['strCompanyPhone'] = mysql_real_escape_string(trim($_POST['strCompanyPhone']));
    $arrTplVars['strCompanyPhone'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyPhone'])));

//    $arrSqlData['strCompanyDescription'] = mysql_real_escape_string(trim($_POST['strCompanyDescription']));
//    $arrTplVars['strCompanyDescription'] = htmlspecialchars(stripslashes(trim($_POST['strCompanyDescription'])));

    if ($intStatusError!=1 && !$_SESSION['isSent']) {
        $_SESSION['isSent'] = true;
        $strSqlFields = ""
            ." jv_title = '{$arrSqlData['strTitle']}'"
            .", jv_salary_from = '{$arrSqlData['strSalaryFrom']}'"
            .", jv_salary_to = '{$arrSqlData['strSalaryTo']}'"
//            .", jv_driver = '$cbxDriver'"
//            .", jv_trips = '$cbxTrips'"
            .($rbSex!='N'?", jv_sex = '$rbSex'":'')
            .", jv_city = '{$arrSqlData['strCity']}'"
            .", jv_description = '{$arrSqlData['strDescription']}'"
            .", jv_contact_fio = '{$arrSqlData['strContactPerson']}'"
            .(!empty($arrSqlData['strCompanyPhone'])?", jv_phone = '{$arrSqlData['strCompanyPhone']}'":'')
            .(!empty($arrSqlData['strCompanyEmail'])?", jv_email = '{$arrSqlData['strCompanyEmail']}'":'')
//            .", jv_jet_id = '$rbEdu'"
//            .", jv_jw_id = '$sbxWorkbusy'"
            .", jv_meta_description = 'Газета Работа от А до Я, вакансия - {$arrSqlData['strTitle']}'"
            .", jv_meta_keywords = '{$arrSqlData['strTitle']},работа от а до я,вакансии в петербурге,резюме петербург и москва,вакансии москва,вакансии спб,работа спб,работа москва,работа 2014,вакансии 2014,резюме 2014'"
            .", jv_status = 'Y'"
        ;
        $strSqlQuery = "INSERT INTO job_vacancies SET $strSqlFields, jv_date_publish = UNIX_TIMESTAMP()";
        if ( !$objDb->query( $strSqlQuery ) ) {
            $intStatusError=1;
            $arrCodeError[] = 'Ошибка базы данных';
        } else {
            $intRecordId = $objDb->insert_id();
            if ( $intRecordId > 0 ) {
                // Добавляем компанию
                $strSqlFields = " jc_title = '".$arrSqlData['strCompany']."'"
//                    .", jc_type = '$rbAgency'"
                    .", jc_city = '{$arrSqlData['strCity']}'"
                    .(!empty($arrSqlData['strAddress'])?", jc_address = '{$arrSqlData['strAddress']}'":'')
                    .(!empty($arrSqlData['strWeb'])?", jc_web = '{$arrSqlData['strWeb']}'":'')
                    .", jc_email = '{$arrSqlData['strCompanyEmail']}'"
                    .", jc_phone = '{$arrSqlData['strCompanyPhone']}'"
                    .", jc_description = '{$arrSqlData['strExp']}'"
                ;
                $strSqlQuery = "INSERT INTO job_companies SET $strSqlFields, jc_date_create = UNIX_TIMESTAMP()";
                if ( !$objDb->query( $strSqlQuery ) ) {
                    $intStatusError=1;
                    $arrCodeError[] = 'Ошибка базы данных! Возможно такая Компания уже существует в нашей базе данных. Если Вы представитель этой компании, Войдите в Личный кабинет и добавьте вакансию.';
                } else {
                    $intCompanyId = $objDb->insert_id();
                    $objVacancy->setId($intRecordId)->setCompanyId($intCompanyId);
                }

                if (!$intStatusError) {
                    $_SESSION['status'] = 'ok';
                    $_SESSION['position'] = $arrTplVars['strTitle'];
                    header('Location: '.SITE_URL.'add-vacancy');
                    exit();
                }
            } else {
                $intStatusError=1;
                $arrCodeError[] = 'Что-то пошло не так! Данные не записаны =(';
            }
        }
    }

    if($intStatusError==1) {
        $arrTplVars['strMessage'] = implode('<br>', $arrCodeError);
        $arrIf['is.msg'] = true;

        if ($_SESSION['isSent']) {
            $_SESSION['isSent'] = false;
            $_SESSION['intStatusError'] = $intStatusError;
            $_SESSION['reloadMessage'] = $arrTplVars['strMessage'];
            header('location: /add-vacancy');
            exit();
        }
    }
}

if (isset($_SESSION['intStatusError']) && $_SESSION['intStatusError']) {
    $intStatusError = $_SESSION['intStatusError'];
    unset($_SESSION['intStatusError']);
}

if (isset($_SESSION['strMessage']) && $_SESSION['strMessage']) {
    $arrTplVars['strMessage'] = $_SESSION['strMessage'];
    $arrIf['is.msg'] = true;
    unset($_SESSION['strMessage']);
}

$arrTplVars['msgStatus'] = intval($intStatusError);

if ($_SESSION['status']=='ok') {
    $arrTplVars['strMessage'] = nl2br("Поздравляем!!!\nВаша вакансия\n<b>".$_SESSION['position']."</b>\nуспешно добавлена!");
    $arrIf['is.msg'] = true;
    unset($_SESSION['status']);
    unset($_SESSION['position']);
}

// Тип занятости
$strSqlQuery = "SELECT * FROM `job_workbusy` ORDER BY jw_id";
$arrWorkbusyTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrWorkbusyTypes) && !empty($arrWorkbusyTypes)) {
    foreach ($arrWorkbusyTypes as $k => $val) {
        $arrWorkbusyTypes[$k]['checked'] = (isset($sbxWorkbusy) && $val['jw_id']==$sbxWorkbusy ? ' selected' : '');
    }
}
$objTpl->tpl_loop("page.content", "workbusy.types", $arrWorkbusyTypes);

// Образование
$strSqlQuery = "SELECT * FROM `job_education_types` ORDER BY jet_id";
$arrEduTypes = $objDb->fetchall($strSqlQuery);
if (is_array($arrEduTypes) && !empty($arrEduTypes)) {
    foreach ($arrEduTypes as $k => $val) {
        $arrEduTypes[$k]['checked'] = (isset($rbEdu) && $val['jet_id']==$rbEdu ? ' checked' : '');
    }
}
$objTpl->tpl_loop("page.content", "education.types", $arrEduTypes);

// Города
$strSqlQuery = "SELECT `city` FROM `site_cities` ORDER BY `pos`, `id`";
$arrCities = $objDb->fetchcol($strSqlQuery);
$arrTplVars['strCities'] = "'".implode("','", $arrCities)."'";// Санкт-Петербург','Москва','Лабытнанги','Салехард'";

$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);

$_SESSION['isSent'] = false;