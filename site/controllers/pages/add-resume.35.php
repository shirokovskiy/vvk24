<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Добавить резюме [add-resume] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "add-resume.35.tpl");

$intStatusError=0;
$arrCodeError[] = 'Возникла ошибка:';

if(isset($_POST) && $_POST['add'] == 'resume') {
    $arrSqlData['strTitle'] = mysql_real_escape_string(trim($_POST['strTitle']));
    $arrTplVars['strTitle'] = htmlspecialchars(stripslashes(trim($_POST['strTitle'])));

    if (empty($arrSqlData['strTitle']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Претендую на должность';
    }

    $arrSqlData['strFio'] = mysql_real_escape_string(trim($_POST['strFio']));
    $arrTplVars['strFio'] = htmlspecialchars(stripslashes(trim($_POST['strFio'])));

    if (empty($arrSqlData['strFio']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Имя';
    }

    $arrSqlData['strBirthday'] = mysql_real_escape_string(trim($_POST['strBirthday']));
    $arrTplVars['strBirthday'] = htmlspecialchars(stripslashes(trim($_POST['strBirthday'])));

    $cbxAgeOnly = ($_POST['cbxAgeOnly'] == 'Y' ? 'Y' : 'N');
    $arrTplVars['cbxAgeOnly'] = ($cbxAgeOnly == 'Y' ? ' checked' : '');

    if ($cbxAgeOnly == 'Y') {
        // количество лет между датами
        $a = new DateTime($arrSqlData['strBirthday']);
        $b = new DateTime(date('Y-m-d'));
        $age = $a->diff($b)->format('%y');
    }

    $cbxMarriage = ($_POST['cbxMarriage'] == 'Y' ? 'Y' : 'N');
    $arrTplVars['cbxMarriage'] = ($cbxMarriage == 'Y' ? ' checked' : '');

    $arrSqlData['strSalary'] = intval(trim($_POST['strSalary']));
    $arrTplVars['strSalary'] = htmlspecialchars(stripslashes(trim($_POST['strSalary'])));

    if (intval($arrSqlData['strSalary']) <= 100) {
        $intStatusError=1;
        $arrCodeError[] = 'Проверьте правильно ли Вы ввели ожидаемую Зряплату';
    }

    $arrSqlData['strCity'] = mysql_real_escape_string(trim($_POST['strCity']));
    $arrTplVars['strCity'] = htmlspecialchars(stripslashes(trim($_POST['strCity'])));

    if (empty($arrSqlData['strCity']) ) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили поле Город';
    }

    $rbSex = $_POST['rbSex'];
    $arrTplVars['rb_'.$rbSex] = " checked";

    if(empty($rbSex)) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не указали пол';
    }

    $rbChildren = $_POST['rbChildren'];
    $arrTplVars['rbChildren_'.$rbChildren] = ' checked';

    $cbxTrips = $_POST['rbTrips'];
    $arrTplVars['rbTrips_'.$cbxTrips] = " checked";

    $arrSqlData['strAbout'] = mysql_real_escape_string(trim($_POST['strAbout']));
    $arrTplVars['strAbout'] = htmlspecialchars(stripslashes(trim($_POST['strAbout'])));

    if (is_array($_POST['mobPhone']) && !empty($_POST['mobPhone'])) {
        $arrSqlData['strPhone'] = implode(' ',$_POST['mobPhone']);
        $arrSqlData['strPhone'] = mysql_real_escape_string(trim($arrSqlData['strPhone']));
    } else {
        $arrSqlData['strPhone'] = mysql_real_escape_string(trim($_POST['strPhone']));
        $arrTplVars['strPhone'] = htmlspecialchars(stripslashes(trim($_POST['strPhone'])));
    }

    if (empty($arrSqlData['strPhone'])) {
        $intStatusError=1;
        $arrCodeError[] = 'Вы не заполнили Телефон';
    }

    $arrSqlData['strEmail'] = mysql_real_escape_string(trim($_POST['strEmail']));
    $arrTplVars['strEmail'] = htmlspecialchars(stripslashes(trim($_POST['strEmail'])));

    if(!empty($arrSqlData['strEmail']) && !$objUtils->checkEmail($arrSqlData['strEmail'])) {
        $intStatusError=1;
        $arrCodeError[] = 'Поле Email содержит не правильного формата адрес';
    }

    $sbxWorkbusy = intval($_POST['sbxWorkbusy']);

    $arrSqlData['strSkype'] = mysql_real_escape_string(trim($_POST['strSkype']));
    $arrTplVars['strSkype'] = htmlspecialchars(stripslashes(trim($_POST['strSkype'])));

    $arrSqlData['strICQ'] = mysql_real_escape_string(trim($_POST['strICQ']));
    $arrTplVars['strICQ'] = htmlspecialchars(stripslashes(trim($_POST['strICQ'])));

    $arrSqlData['strLinkedin'] = mysql_real_escape_string(trim($_POST['strLinkedin']));
    $arrTplVars['strLinkedin'] = htmlspecialchars(stripslashes(trim($_POST['strLinkedin'])));

    $cbxDriver = ($_POST['cbxDriver'] == 'Y' ? 'Y' : 'N');
    $arrTplVars['cbxDriver'] = ($cbxDriver == 'Y' ? ' checked' : '');

    $arrSqlData['strInstitution'] = mysql_real_escape_string(trim($_POST['strInstitution']));
    $arrTplVars['strInstitution'] = htmlspecialchars(stripslashes(trim($_POST['strInstitution'])));

    $arrSqlData['strFaculty'] = mysql_real_escape_string(trim($_POST['strFaculty']));
    $arrTplVars['strFaculty'] = htmlspecialchars(stripslashes(trim($_POST['strFaculty'])));

    $arrSqlData['strSpecialization'] = mysql_real_escape_string(trim($_POST['strSpecialization']));
    $arrTplVars['strSpecialization'] = htmlspecialchars(stripslashes(trim($_POST['strSpecialization'])));

    $arrSqlData['strEduDateFin'] = mysql_real_escape_string(trim($_POST['strEduDateFin']));
    $arrTplVars['strEduDateFin'] = htmlspecialchars(stripslashes(trim($_POST['strEduDateFin'])));

    $arrSqlData['strEduPlus'] = mysql_real_escape_string(trim($_POST['strEduPlus']));
    $arrTplVars['strEduPlus'] = htmlspecialchars(stripslashes(trim($_POST['strEduPlus'])));

    if ($intStatusError!=1) {
        $strSqlFields = ""
            ." jr_title = '{$arrSqlData['strTitle']}'"
            .", jr_salary = '{$arrSqlData['strSalary']}'"
            .", jr_fio = '{$arrSqlData['strFio']}'"
            . ($age>0?", jr_age = $age":'')
            . (is_object($a) ? ", jr_birthday = '".$a->format('Y-m-d')."'" : '' )
            .", jr_sex = '".$rbSex."'"
            .", jr_children = '".$rbChildren."'"
            .", jr_marriage = '".$cbxMarriage."'"
            .", jr_city = '".$arrSqlData['strCity']."'"
            .", jr_driver = '$cbxDriver'"
            .", jr_work_busy = '{$sbxWorkbusy}'"
            .", jr_trips = '{$cbxTrips}'"
            .", jr_phone = '{$arrSqlData['strPhone']}'"
            .", jr_email = '{$arrSqlData['strEmail']}'"
            .", jr_skype = '{$arrSqlData['strSkype']}'"
            .", jr_icq = '{$arrSqlData['strICQ']}'"
            .", jr_linkedin = '{$arrSqlData['strLinkedin']}'"
            .", jr_edu_plus = '{$arrSqlData['strEduPlus']}'"
            .", jr_about = '{$arrSqlData['strAbout']}'"
            .", jr_status = 'N'"
        ;
        $strSqlQuery = "INSERT INTO job_resumes SET ".$strSqlFields.", jr_date_publish = UNIX_TIMESTAMP()";
//        $objDb->setDebugModeOn();
        if ( !$objDb->query( $strSqlQuery ) ) {
            $intStatusError=1;
            $arrCodeError[] = 'Ошибка базы данных: '.$strSqlQuery;
        } else {
            $intResumeId = $objDb->insert_id();
            if ( $intResumeId > 0 ) {
                /**
                 * Work history save
                 */
                $arrSqlData['strCompany'] = mysql_real_escape_string(trim($_POST['strCompany']));
                $arrTplVars['strCompany'] = htmlspecialchars(stripslashes(trim($_POST['strCompany'])));

                $arrSqlData['strPosition'] = mysql_real_escape_string(trim($_POST['strPosition']));
                $arrTplVars['strPosition'] = htmlspecialchars(stripslashes(trim($_POST['strPosition'])));

                $arrSqlData['strDateFrom'] = mysql_real_escape_string(trim($_POST['strDateFrom']));
                $arrSqlData['strDateFrom'] = $objUtils->workDate(13, $arrSqlData['strDateFrom']);
                $arrTplVars['strDateFrom'] = htmlspecialchars(stripslashes(trim($_POST['strDateFrom'])));

                $arrSqlData['strDateTo'] = mysql_real_escape_string(trim($_POST['strDateTo']));
                $arrSqlData['strDateTo'] = $objUtils->workDate(13, $arrSqlData['strDateTo']);
                $arrTplVars['strDateTo'] = htmlspecialchars(stripslashes(trim($_POST['strDateTo'])));

                $arrSqlData['strRegion'] = mysql_real_escape_string(trim($_POST['strRegion']));
                $arrTplVars['strRegion'] = htmlspecialchars(stripslashes(trim($_POST['strRegion'])));

                $arrSqlData['strWeb'] = mysql_real_escape_string(trim($_POST['strWeb']));
                $arrTplVars['strWeb'] = htmlspecialchars(stripslashes(trim($_POST['strWeb'])));

                $arrSqlData['strResponsibility'] = mysql_real_escape_string(trim($_POST['strResponsibility']));
                $arrTplVars['strResponsibility'] = htmlspecialchars(stripslashes(trim($_POST['strResponsibility'])));

                $arrSqlData['intCategoryId'] = $intCategoryId = intval(trim($_POST['intCategoryId']));
                $arrTplVars['intCategoryId'] = ($arrSqlData['intCategoryId'] > 0 ? $arrSqlData['intCategoryId'] : '');

                if (!empty($arrSqlData['strCompany']) && !empty($arrSqlData['strDateFrom']) && !empty($arrSqlData['strPosition'])) {
                    $strSqlFields = " jrw_jr_id = ".$intResumeId
                        .", jrw_jc_id = ".$arrSqlData['intCategoryId']
                        .", jrw_position = '{$arrSqlData['strPosition']}'"
                        .", jrw_company = '{$arrSqlData['strCompany']}'"
                        .", jrw_region = '{$arrSqlData['strRegion']}'"
                        .", jrw_web = '{$arrSqlData['strWeb']}'"
                        . ($objUtils->dateValid($arrSqlData['strDateFrom']) ? ", jrw_date_start = '{$arrSqlData['strDateFrom']}'" : '')
                        . ($objUtils->dateValid($arrSqlData['strDateTo']) ? ", jrw_date_fin = '{$arrSqlData['strDateTo']}'" : '')
                        .", jrw_responsibility = '{$arrSqlData['strResponsibility']}'"
                    ;
                    $strSqlQuery = "INSERT INTO job_resumes_works SET ".$strSqlFields;
                    if ( !$objDb->query( $strSqlQuery ) ) {
                        $intStatusError=1;
                        $arrCodeError[] = 'Ошибка базы данных: '.$strSqlQuery;
                    }
                }


                /**
                 * Education save
                 */
                $rbEdu = intval($_POST['rbEdu']);

                $arrSqlData['strInstitution'] = mysql_real_escape_string(trim($_POST['strInstitution']));
                $arrTplVars['strInstitution'] = htmlspecialchars(stripslashes(trim($_POST['strInstitution'])));

                $arrSqlData['strFaculty'] = mysql_real_escape_string(trim($_POST['strFaculty']));
                $arrTplVars['strFaculty'] = htmlspecialchars(stripslashes(trim($_POST['strFaculty'])));

                $arrSqlData['strSpecialization'] = mysql_real_escape_string(trim($_POST['strSpecialization']));
                $arrTplVars['strSpecialization'] = htmlspecialchars(stripslashes(trim($_POST['strSpecialization'])));

                $arrSqlData['strEduDateStart'] = mysql_real_escape_string(trim($_POST['strEduDateStart']));
                $arrTplVars['strEduDateStart'] = htmlspecialchars(stripslashes(trim($_POST['strEduDateStart'])));

                $arrSqlData['strEduDateFin'] = mysql_real_escape_string(trim($_POST['strEduDateFin'])); // обычная дата, не timestamp
                $arrTplVars['strEduDateFin'] = htmlspecialchars(stripslashes(trim($_POST['strEduDateFin'])));

                if (!empty($rbEdu) && !empty($arrSqlData['strInstitution']) && !empty($arrTplVars['strEduDateFin'])) {
                    $strSqlFields = " jre_jr_id = ".$intResumeId
                        .", jre_jet_id = ".$rbEdu
                        .", jre_institution = '{$arrSqlData['strInstitution']}'"
                        .", jre_faculty = '{$arrSqlData['strFaculty']}'"
                        .", jre_spec = '{$arrSqlData['strSpecialization']}'"
                        . ($objUtils->dateValid(date('Y-m-d',strtotime($arrSqlData['strEduDateStart']))) ? ", jre_date_start = '".$objUtils->workDate(13, $arrSqlData['strEduDateStart'])."'" : '')
                        . ($objUtils->dateValid(date('Y-m-d',strtotime($arrSqlData['strEduDateFin']))) ? ", jre_date_fin = '".$objUtils->workDate(13, $arrSqlData['strEduDateFin'])."'" : '')
                    ;
                    $strSqlQuery = "INSERT INTO job_resumes_education SET ".$strSqlFields;
                    if ( !$objDb->query( $strSqlQuery ) ) {
                        $intStatusError=1;
                        $arrCodeError[] = 'Ошибка базы данных: '.$strSqlQuery;
                    }
                }

                if (!$intStatusError) {
                    $_SESSION['status'] = 'ok';
                    $_SESSION['position'] = $arrTplVars['strTitle'];
                    header('Location: '.SITE_URL.'add-resume');
                    exit();
                }
            }
        }
    }

    if($intStatusError==1) {
        $arrTplVars['strMessage'] = implode('<br>', $arrCodeError);
        $arrIf['is.msg'] = true;
    }
} else {
    $arrTplVars['strSalary'] = '35000';
}
$arrTplVars['msgStatus'] = intval($intStatusError);

if ($_SESSION['status']=='ok') {
    $arrTplVars['strMessage'] = nl2br("Ваше резюме\n<b>".$_SESSION['position']."</b>\nуспешно добавлено!");
    $arrIf['is.msg'] = true;
    unset($_SESSION['status']);
    unset($_SESSION['position']);
}

// Сфера деятельности
$strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` = 0 OR `jc_parent` IS NULL ORDER BY `jc_order`";
$arrCats = $objDb->fetchall($strSqlQuery);
if (is_array($arrCats) && !empty($arrCats)) {
    foreach ($arrCats as $k => $arr) {
        $arrCats[$k]['strOptGroupName'] = htmlspecialchars($arr['jc_title']);
        $arrCats[$k]['intGroupID'] = $arr['jc_id'];
    }
}
$objTpl->tpl_loop("page.content", "option.group.category", $arrCats);
if (is_array($arrCats) && !empty($arrCats)) {
    foreach ($arrCats as $k => $arr) {
        $strSqlQuery = "SELECT * FROM `job_categories` WHERE `jc_parent` = ".$arr['jc_id']." ORDER BY `jc_order`";
        $arrSubCats = $objDb->fetchall($strSqlQuery);

        if (is_array($arrSubCats) && !empty($arrSubCats)) {
            foreach ($arrSubCats as $kk => $varr) {
                $arrSubCats[$kk]['intOptionValue'] = intval($varr['jc_id']);
                $arrSubCats[$kk]['strOptionTitle'] = htmlspecialchars($varr['jc_title']);
                $arrSubCats[$kk]['selected'] = ($varr['jc_id']==$intCategoryId ? ' selected' : '');
            }
        }

        $objTpl->tpl_loop("page.content", "group.options.".$arr['jc_id'], $arrSubCats);
    }
}

// Города
$strSqlQuery = "SELECT `city` FROM `site_cities` ORDER BY `pos`, `id`";
$arrCities = $objDb->fetchcol($strSqlQuery);
$arrTplVars['strCities'] = "'".implode("','", $arrCities)."'";// Санкт-Петербург','Москва','Лабытнанги','Салехард'";

// Тип занятости
$strSqlQuery = "SELECT * FROM `job_workbusy` ORDER BY jw_id";
$arrWorkbusyTypes = $objDb->fetchall($strSqlQuery);
$objTpl->tpl_loop("page.content", "workbusy.types", $arrWorkbusyTypes);

// Образование
$strSqlQuery = "SELECT * FROM `job_education_types` ORDER BY jet_id";
$arrEduTypes = $objDb->fetchall($strSqlQuery);
$objTpl->tpl_loop("page.content", "education.types", $arrEduTypes);

$arrIf['no.image'] = !$arrIf['exist.image'];
$arrTplVars['strCity'] = (!empty($arrTplVars['strCity'])?$arrTplVars['strCity']:'Санкт-Петербург');
$arrTplVars['rbTrips_N'] = (!isset($arrTplVars['rbTrips_Y']) && !isset($arrTplVars['rbTrips_S'])?' checked':'');

$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);

/*
 * Тестовые входные данные
Array
(
    [add] => resume
    [fio] => Широковский Дмитрий Анатольевич
    [rbSex] => male
    [dob] => 05.07.1977
    [city_live] => Санкт-Петербург
    [email] => jimmy.webstudio@gmail.com
    [mobPhone] => Array
        (
            [0] => +7
            [1] => 921
            [2] => 6419057
            [3] => с часу до двух
        )

    [homePhone] => Array
        (
            [0] =>
            [1] =>
            [2] =>
            [3] =>
        )

    [workPhone] => Array
        (
            [0] =>
            [1] =>
            [2] =>
            [3] =>
        )

    [position] => Программист
    [proff] => IT
    [salary] => 30000
    [choose-schedule-work] => any
    [strExpCity] => Санкт-Петербург
    [choose-work-trip] => yes
    [intExp] => 10
    [company_name] => Сбондс
    [position_title] => Программист
    [jobPeriod] => Array
        (
            [0] => 2007
            [1] => 2009
        )

    [about_job] => Программировал для Сбондс
    [education-type] => fh
    [school_title] => ИТМО
    [education] => Инженер
    [strEduFrom] => 1996
    [strEduTo] => 2002
    [speciality] => Конструктор-технолог ЭВС
    [description] => учиться было здорово
    [strLang] => Русский
    [moreLang] => Array
        (
            [0] => Английский
        )

    [moreLangLevel] => Array
        (
            [0] => Intermediate
        )

    [experience_description] => могу копать, могу не копать
    [about] => молодец
    [citizenship] => РФ
)

*/
