<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: Резюме [resume] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "resume.49.tpl");

define('NOINFO', '<i class="light_grey">нет информации</i>');
$arrIf['previous.works'] =
$arrIf['education'] = false;

/**
 * Если указан параметр записи
 */
if (isset($arrReqUri[1]) && !empty($arrReqUri[1])) {
    $intRecordId = intval($arrReqUri[1]);

    if ($intRecordId > 0) {
        //
        $strSqlQuery = "SELECT * FROM `job_resumes` WHERE `jr_id` = ".$intRecordId." AND `jr_status` = 'Y'";
        $arrRecord = $objDb->fetch($strSqlQuery);

        if (is_array($arrRecord) && !empty($arrRecord)) {
            $arrTplVars['intResumeId'] = $intRecordId;
            $arr_string_fields = array('strTitle'=>'jr_title', 'strFio'=>'jr_fio', 'strLinkedin'=>'jr_linkedin', 'strSkype'=>'jr_skype', 'strEmail'=>'jr_email', 'strPhone'=>'jr_phone', 'strCity'=>'jr_city', 'strAbout'=>'jr_about', 'strEduPlus'=>'jr_edu_plus');
            $arr_checked_fields = array('jr_sex', 'jr_marriage', 'jr_children', 'jr_trips', 'jr_driver');
            $arr_int_fields = array('strSalary'=>'jr_salary', 'strIcq'=>'jr_icq', 'strAge'=>'jr_age');

            foreach($arr_string_fields as $key => $field) {
                $arrRecord[$key] = (!empty($arrRecord[$field]) ? htmlspecialchars($arrRecord[$field]) : NOINFO);
                $arrIf['is.'.$key] = $arrRecord[$key] != NOINFO;
            }

            if ($arrRecord['strPhone']==NOINFO) {
                if ($arrRecord['jr_grab_has_phone']=='Y' && $arrRecord['jr_grab_src_id'] > 0) {
                    if (file_exists(PRJ_IMAGES.'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id'].'.jpg') && filesize(PRJ_IMAGES.'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id'].'.jpg') > 0) {
                        $arrRecord['strPhone'] = '<img src="'.$arrTplVars['cfgAllImg'].'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id'].'.jpg" class="phone_snap"/>';
                        $arrIf['is.strPhone'] = true;
                    } elseif (file_exists(PRJ_IMAGES.'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id']) && filesize(PRJ_IMAGES.'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id']) > 0) {
                        $arrRecord['strPhone'] = '<img src="'.$arrTplVars['cfgAllImg'].'resumes/contactimg/phone.'.$arrRecord['jr_grab_src_id'].'" class="phone_snap"/>';
                        $arrIf['is.strPhone'] = true;
                    }
                }
            }

            if ($arrRecord['strEmail']==NOINFO) {
                if ($arrRecord['jr_grab_has_email']=='Y' && $arrRecord['jr_grab_src_id'] > 0) {
                    if (file_exists(PRJ_IMAGES.'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id'].'.jpg') && filesize(PRJ_IMAGES.'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id'].'.jpg') > 0) {
                        $arrRecord['strEmail'] = '<img src="'.$arrTplVars['cfgAllImg'].'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id'].'.jpg" class="email_snap"/>';
                        $arrIf['is.strEmail'] = true;
                    } elseif (file_exists(PRJ_IMAGES.'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id']) && filesize(PRJ_IMAGES.'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id']) > 0) {
                        $arrRecord['strEmail'] = '<img src="'.$arrTplVars['cfgAllImg'].'resumes/contactimg/email.'.$arrRecord['jr_grab_src_id'].'" class="email_snap"/>';
                        $arrIf['is.strEmail'] = true;
                    }
                }
            }

            if (!empty($arrRecord['strAbout'])) $arrRecord['strAbout'] = nl2br($arrRecord['strAbout']);

            foreach($arr_checked_fields as $field) {
                $arrRecord[$field.'_'.$arrRecord[$field]] = ' checked';
            }

            foreach($arr_int_fields as $key => $field) {
                $arrRecord[$key] = (intval($arrRecord[$field]) ? $arrRecord[$field] : NOINFO);
            }

            $arrRecord['strTitle'] = mb_ucfirst($arrRecord['strTitle']);
            $arrTplVars['m_title'] = $arrRecord['strTitle'];

            if (empty($arrRecord['jr_age']) && !empty($arrRecord['jr_birthday'])) {
                // количество лет между датами
                $a = new DateTime($arrRecord['jr_birthday']);
                $b = new DateTime(date('Y-m-d'));
                $arrRecord['strAge'] = $a->diff($b)->format('%y');
            }
            $arrIf['is.strAge'] = !empty($arrRecord['strAge']) && $arrRecord['strAge'] != NOINFO;
            $arrIf['is.strSalary'] = !empty($arrRecord['strSalary']) && $arrRecord['strSalary'] != NOINFO;
            if ($arrIf['is.strSalary']) {
                $arrRecord['strSalary'] .= ' руб';
            }

            $arrRecord['strMarriage'] = $arrRecord['jr_marriage'] == 'Y' ? 'женат/замужем' : 'холост/не замужем';
            $arrRecord['strChildren'] = $arrRecord['jr_children'] == 'Y' ? 'есть' : 'нет';
            $arrRecord['strDriver'] = $arrRecord['jr_driver'] == 'Y' ? 'есть' : 'не имею';
            $arrRecord['strTrips'] = $arrRecord['jr_trips'] == 'Y' ? 'да, в любое время' : ($arrRecord['jr_trips'] == 'S' ? 'иногда / не часто': 'не готов(-а)');

            if ($arrRecord['jr_work_busy']) {
                include_once "models/JobWorkbusy.php";
                $objWorkbusy = new JobWorkbusy($_db_config);
                $arrRecord['strWorkbusy'] = $objWorkbusy->getStatus($arrRecord['jr_work_busy']);
            } else {
                $arrRecord['strWorkbusy'] = NOINFO;
            }
            $arrIf['is.strWorkbusy'] = $arrRecord['strWorkbusy'] != NOINFO;

            if (date('Y', $arrRecord['jr_date_publish']) > 2012)
                $arrRecord['strDatePublish'] = date("d.m.Y", $arrRecord['jr_date_publish']);

            include_once "models/JobCategory.php";
            $objCategory = new JobCategory($_db_config);

            /**
             * Выборка о работе
             */
            $strSqlQuery = "SELECT * FROM `job_resumes_works` WHERE `jrw_jr_id` = ".$intRecordId;
            $arrWorksHistory = $objDb->fetchall($strSqlQuery);
            if (is_array($arrWorksHistory) && !empty($arrWorksHistory)) {
                $arrIf['previous.works'] = true;

                foreach ($arrWorksHistory as $k => $arr) {
                    $arrWorksHistory[$k]['strHistoryCompany'] = (!empty($arr['jrw_company']) ? htmlspecialchars($arr['jrw_company']) : '&mdash;' );
                    $arrWorksHistory[$k]['strHistoryCategory'] = $objCategory->getCategoryFieldById($arr['jrw_jc_id'], 'jc_title');
                    $arrWorksHistory[$k]['strHistoryPosition'] = (!empty($arr['jrw_position']) ? htmlspecialchars($arr['jrw_position']) : '&mdash;' );
                    $arrWorksHistory[$k]['strHistoryRegion'] = (!empty($arr['jrw_region']) ? htmlspecialchars($arr['jrw_region']) : '&mdash;' );
                    $arrWorksHistory[$k]['strHistoryWeb'] = '&mdash;';
                    if (!empty($arr['jrw_web'])) {
                        $arr['jrw_web'] = str_replace('http://', '', $arr['jrw_web']);
                        $arr['jrw_web'] = str_replace('https://', '', $arr['jrw_web']);
                        $arrWorksHistory[$k]['strHistoryWeb'] = '<a href="http://'.$arr['jrw_web'].'" target=_blank>'. $arr['jrw_web'] .'</a>';
                    }
                    $arrWorksHistory[$k]['strHistoryResponsibilities'] = (!empty($arr['jrw_responsibility']) ? htmlspecialchars($arr['jrw_responsibility']) : '&mdash;' );
                    $arrWorksHistory[$k]['strHistoryDateFrom'] = $objUtils->workDate(5, $arr['jrw_date_start']);
                    $arrWorksHistory[$k]['strHistoryDateTo'] = ($objUtils->dateValid($arr['jrw_date_fin']) ? $objUtils->workDate(5, $arr['jrw_date_fin']) : '&mdash;' );
                }
            }

            /**
             * Образование
             */
            include_once "models/JobEduType.php";
            $objEduType = new JobEduType();

            $intEduType = $objEduType->getIdByResume($intRecordId);

            if ($intEduType) {
                $arrRecord['strEducation'] = $objEduType->getStatus($intEduType);
                $arrIf['is.strEducation'] = true;
            } else {
                $arrRecord['strEducation'] = NOINFO;
            }

            $arr_string_fields = array('strInstitution'=>'jre_institution', 'strFaculty'=>'jre_faculty', 'strSpecialization'=>'jre_spec');
            $strSqlQuery = "SELECT * FROM `job_resumes_education` WHERE `jre_jr_id` = ".$intRecordId;
            $arrEdu = $objDb->fetch($strSqlQuery);
            if (is_array($arrEdu) && !empty($arrEdu)) {
                $arrIf['education'] = true;
                foreach($arr_string_fields as $key => $field) {
                    $arrRecord[$key] = (!empty($arrEdu[$field]) ? htmlspecialchars($arrEdu[$field]) : NOINFO);
                    $arrIf['is.'.$key] = $arrRecord[$key] != NOINFO;
                }

                if (!empty($arrEdu['jre_date_start']) && $objUtils->dateValid($arrEdu['jre_date_start'])) {
                    $arrRecord['strEduDateStart'] = date('d.m.Y', strtotime($arrEdu['jre_date_start']));
                } else {
                    $arrRecord['strEduDateStart'] = NOINFO;
                }

                if (!empty($arrEdu['jre_date_fin']) && $objUtils->dateValid($arrEdu['jre_date_fin'])) {
                    $arrRecord['strEduDateFin'] = date('d.m.Y', strtotime($arrEdu['jre_date_fin']));
                } else {
                    $arrRecord['strEduDateFin'] = 'по наст.вр.';
                }
            }

            $objTpl->tpl_array("page.content", $arrRecord);
        }
    }
}
$objTpl->tpl_loop("page.content", "resume.history", $arrWorksHistory);
$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
