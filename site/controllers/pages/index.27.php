<?php
/** Created by phpWebStudio(c) 2013 (Shirokovskiy Dmitry aka Jimmy™).
 * Страница: ВВК24 [index] */
$objTpl->Template(SITE_TPL_PAGE_DIR);
$objTpl->tpl_load("page.content", "index.27.tpl");

// Выборка ведущих компаний
$strSqlQuery = "SELECT jc_id, jc_title, si_filename FROM `job_companies` LEFT JOIN `site_images` ON (si_type = 'company' AND si_rel_id = jc_id) WHERE `jc_top` = 'Y' AND `jc_status` = 'Y'";
$arrComps = $objDb->fetchall($strSqlQuery);
$arrIf['a.lot.of.companies'] = false;
if (is_array($arrComps) && !empty($arrComps)) {
    $arrIf['a.lot.of.companies'] = count($arrComps) > 8;
    foreach ($arrComps as $key => $value) {
        $arrComps[$key]['jc_title'] = htmlspecialchars($value['jc_title'], ENT_QUOTES);
        if (empty($value['si_filename']) || !file_exists(PRJ_IMAGES.'companies/'.$value['si_filename'])) {
            unset($arrComps[$key]);
        }
    }
}
$objTpl->tpl_loop("page.content", "list.top.companies", $arrComps); unset($arrComps);

$countVac =
$countRes = 28;

if (isset($_SESSION['myCity']) && !empty($_SESSION['myCity'])) {
    $strSqlWhereV = " AND jv_city LIKE '%".$_SESSION['myCity']."%'";
    $strSqlWhereR = " AND jr_city LIKE '%".$_SESSION['myCity']."%'";
}


// Последние вакансии
$strSqlQuery = "SELECT jv_id, jv_title, jv_salary_from, jv_salary_to, jv_phone, jv_email, jv_city FROM `job_vacancies` WHERE `jv_status` = 'Y' $strSqlWhereV AND (`jv_email` NOT LIKE '' OR `jv_email` IS NOT NULL) AND (`jv_phone` NOT LIKE '' OR `jv_phone` IS NOT NULL) ORDER BY `jv_id` DESC LIMIT ".$countVac;
$arrVac = $objDb->fetchall($strSqlQuery);
if (is_array($arrVac) && !empty($arrVac)) {
    foreach ($arrVac as $k => $vacancy) {
        $arrVac[$k]['jv_title'] = mb_ucfirst($objUtils->substrText($vacancy['jv_title'], 80, true));
        $arrVac[$k]['jv_city'] = mb_ucfirst($objUtils->substrText($vacancy['jv_city'], 80, true));
        $arrVac[$k]['strSalary'] = (!empty($vacancy['jv_salary_from']) ? 'от '.$vacancy['jv_salary_from'] : '').(!empty($vacancy['jv_salary_to']) ? (!empty($vacancy['jv_salary_from'])?' до ':'').$vacancy['jv_salary_to'] : '');
        if (!empty($arrVac[$k]['strSalary'])) {
            $arrVac[$k]['strSalary'] .= ' руб';
        } elseif (!empty($vacancy['jv_salary'])) {
            $arrVac[$k]['strSalary'] = $vacancy['jv_salary'];
        }

        if (empty($arrVac[$k]['strSalary'])) {
            $arrVac[$k]['strSalary'] = 'зарплата не указана';
        }
        $arrVac[$k]['strPhone'] = (!empty($vacancy['jv_phone']) ? $vacancy['jv_phone'] : '');
        $arrVac[$k]['strEmail'] = (!empty($vacancy['jv_email']) ? $vacancy['jv_email'] : '');
    }
} else {
    $arrIf['list.top.vacancies'] = true;
}
$objTpl->tpl_loop("page.content", "list.top.vacancies", $arrVac);
unset($arrVac);

// Последние резюме
$strSqlQuery = "SELECT jr_id, jr_title, jr_salary, jr_phone, jr_email FROM `job_resumes` WHERE `jr_status` = 'Y' $strSqlWhereR AND `jr_salary` != '' AND `jr_salary` IS NOT NULL  AND (`jr_email` NOT LIKE '' OR `jr_email` IS NOT NULL) AND (`jr_phone` NOT LIKE '' OR `jr_phone` IS NOT NULL) ORDER BY `jr_id` DESC LIMIT ".$countRes;
$arrRes = $objDb->fetchall($strSqlQuery);
if (is_array($arrRes) && !empty($arrRes)) {
    foreach ($arrRes as $k => $resume) {
        $arrRes[$k]['jr_title'] = mb_ucfirst($objUtils->substrText($resume['jr_title'], 80, true));
        if(!empty($resume['jr_salary']) && intval($resume['jr_salary'])) {
            $arrRes[$k]['strSalary'] = 'от '.$resume['jr_salary'].' руб';
        } else {
            $arrRes[$k]['strSalary'] = 'зарплата не указана';
        }
        $arrRes[$k]['strPhone'] = (!empty($resume['jr_phone']) ? $resume['jr_phone'] : '');
        $arrRes[$k]['strEmail'] = (!empty($resume['jr_email']) ? $resume['jr_email'] : '');
    }
} else {
    $arrIf['list.top.resumes'] = true;
}
$objTpl->tpl_loop("page.content", "list.top.resumes", $arrRes);

$arrTplVars['strMyCity'] = $_SESSION['myCity'];

$objTpl->tpl_if("page.content", $arrIf);
$objTpl->tpl_array("page.content", $arrTplVars);
