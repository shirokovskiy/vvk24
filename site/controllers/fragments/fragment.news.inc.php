<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2013.11 */
///+++ Обработчик фрагмента: Новости [fragment.news]
$arrTplVars['name.fragment'] = 'fragment.news';
$objTpl->tpl_load($arrTplVars['name.fragment'], "fragment.news.frg");

/**
 * Выборка новостей
 */
$strSqlQuery = "SELECT * FROM `site_news` WHERE `sn_status` = 'Y' AND `sn_date_publ` <= DATE_FORMAT(NOW(), '%Y-%m-%d') ORDER BY `sn_date_publ` DESC, sn_id DESC LIMIT 3";
$arrList = $objDb->fetchall($strSqlQuery);

if (is_array($arrList) && !empty($arrList)) {
    foreach ($arrList as $key => $value) {
        $arrList[$key]['strNewsTitle'] = htmlspecialchars($value['sn_title']);
        $arrList[$key]['strNewsDate'] = ($value['sn_date_publ']==date('Y-m-d') ? 'Сегодня' : $objUtils->workDate(3, $value['sn_date_publ']));
        $arrList[$key]['strNewsLink'] = $value['sn_url'];
    }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "list.news", $arrList);
$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
