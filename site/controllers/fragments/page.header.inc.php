<?php /* CMS - PhpWebStudio (Dmitry Shirokovskiy) Copyright 2006.10 - 2013.11 */
include_once "models/SiteUser.php";
include_once "GeoData/GeoIP/geoip.inc";
include_once "GeoData/GeoIP/geoipcity.inc";
include_once "GeoData/GeoIP/geoipregionvars.php";

///+++ Обработчик фрагмента: Заголовок страниц [page.header]
$arrTplVars['name.fragment'] = 'page.header';
$objTpl->tpl_load($arrTplVars['name.fragment'], "page.header.frg");

if (!isset($objSiteUser) || !is_object($objSiteUser)) {
    $objSiteUser = new SiteUser();
}

/**
 * Geo-Location
 */
if (!isset($_SESSION['myCityId'])) {
    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '188.134.80.143' : $_SERVER['REMOTE_ADDR'];
    $myIP = $_SERVER['REMOTE_ADDR']=='127.0.0.1' ? '66.249.78.29' : $_SERVER['REMOTE_ADDR']; // Beverly Hills
    $objGeoIP = geoip_open(GEOIPCITY_DAT, GEOIP_STANDARD);
    $rcGeoIP = geoip_record_by_addr($objGeoIP, $myIP);

    if ( !is_null($rcGeoIP) ) { // Office IP = 188.64.144.36
        $intCityId = $objSiteUser->saveIPCityCountry($myIP, $rcGeoIP);

        if ($intCityId > 0) {
            $_SESSION['myCityId'] = $intCityId;
            $city = $objSiteUser->getCityById($intCityId);
            $_SESSION['myCity'] = $city['city']; // get Russian name (city_eng is English name)
            unset($city);
        }
    }

    geoip_close($objGeoIP);
}

/**
 * Выбор регионов
 */
$strSqlQuery = "SELECT DISTINCT(`jv_city`) AS `strCity` FROM `job_vacancies` WHERE "
    ." CHAR_LENGTH(`jv_city`) > 3 AND `jv_city` NOT REGEXP ',' AND `jv_city` NOT REGEXP '[.]'"
    ." AND `jv_city` NOT LIKE 'Санкт-Петербург'"
    ." AND `jv_city` NOT LIKE 'Москва'"
    ." AND `jv_city` NOT LIKE '%область%'"
    ." AND `jv_city` NOT LIKE '%край%'"
    ." AND `jv_status` = 'Y' LIMIT 44";
$arrList = $objDb->fetchall($strSqlQuery);

if (is_array($arrList) && !empty($arrList)) {
    foreach ($arrList as $k => $arr) {
        $arrList[$k]['strCityUrl'] = urlencode($arr['strCity']);
    }
}
$objTpl->tpl_loop($arrTplVars['name.fragment'], "list.cities", $arrList);

if (empty($arrTplVars['strMyCity'])) {
    if (!empty($_SESSION['myCity'])) {
        $arrTplVars['strMyCity'] = $_SESSION['myCity'];
    } else
        $arrTplVars['strMyCity'] = "Санкт-Петербург";
}

$arrTplVars['isUnknownRegion'] = 'false'; // it means region is known

if ($arrTplVars['strMyCity'] != "Санкт-Петербург" && $arrTplVars['strMyCity'] != "Москва" && !$_SESSION['isCityConfirmed']) {
    $arrTplVars['isUnknownRegion'] = 'true';
    $arrTplVars['strDetectedRegionName'] = $arrTplVars['strMyCity'];
}

$objTpl->tpl_array($arrTplVars['name.fragment'], $arrTplVars);
$objTpl->tpl_if($arrTplVars['name.fragment'], $arrIf);
