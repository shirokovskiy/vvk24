<div id="header">
    <div id="top_region_link">Отличная работа в городе: <a href="javascript: void(0)" class="select_region">{strMyCity}</a></div>
    <div id="login_box"><if authed><a href="{cfgSiteUrl}profile">Личный кабинет</a></if authed><ifelse authed><a href="javascript: void(0)" class="auth_form_trigger" rel="#overlay_box">Войти</a></ifelse authed> / <if authed><a href="{cfgSiteUrl}profile?logout=1">Выйти</a></if authed><ifelse authed><a href="{cfgSiteUrl}registration">Создать личный кабинет</a></ifelse authed></div>
    <div id="logo_box">
        <a id="logo" href="{cfgSiteUrl}"><img src="/storage/siteimg/logo.png" alt="VVK24.ru"/></a>
        <div id="phone_box">
            <span class="small">(812)</span>
            <span class="big">305-06-90</span>
        </div>
    </div>

    <div id="regions_box">
        <p>Выберите Ваш город:</p>
        <ul>
            <li><a href="#" style="font-weight: bold">Санкт-Петербург</a></li>
            <li><a href="#" style="font-weight: bold">Москва</a></li>
            <loop list.cities>
                <li><a href="#">{strCity}</a></li>
            </loop list.cities>
        </ul>
    </div>
</div>

<div id="overlay_box" class="overlay">
    <div class="contentWrap">
        <form action="/profile" method="POST">
            <input type="hidden" name="auth" value="true"/>
            <h1 class="title">Личный кабинет</h1>
            <h4>Email</h4>
            <input type="text" id="strAuthLogin" name="strAuthLogin" class="plane" maxlength="255" placeholder="ваш-адрес@email.ru"/>
            <h4>Пароль</h4>
            <input type="password" id="strAuthPassword" name="strAuthPassword" class="plane" maxlength="255" />
            <button>Войти</button>
        </form>
    </div>
</div>

<script type="text/javascript">
    jQuery(document).ready(function ($) {
        var sizeW = 245; /* also see in jq.tools.css for #overlay rules */
        var sizeH = 160;

        // Overlay
        function authOverlay() {
            // вычисление размеров для всплывающего слоя
            $('#overlay_box').css('width',sizeW);
            // overlay: loading external pages - if the function argument is given to overlay, it is assumed to be the onBeforeLoad event listener
            $(".auth_form_trigger").overlay({
                top: ($(window).height() - sizeH)/2,
                left: ($(window).width() - sizeW)/2 - 50,
                mask: '#939393',
                fixed: false,
                closeOnClick: true
            });
        }

        authOverlay();

        /**
         * Вызываем меню выбора региона
         */
        $('.select_region').click(function () {
            $('#regions_box').fadeIn();
        });

        /**
         * Выбор региона по клику
         */
        $('#regions_box a').click(function () {
            $('#regions_box').hide();
            $.post("/lib/api/region.php", { city: $(this).text() }, function(data){
                if (data.status == 0)
                {
                    $('.select_region').text( data.city );
                    var myHost = getMyHost();
                    gotoUrl( myHost );
                }
            }, "json");
        });
    });
</script>

<frg top.navigation>
