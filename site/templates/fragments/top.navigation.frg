<div id="navigation" class="clearer">
    <div class="fl">
        <ul class="inline">
            <li><a href="{cfgSiteUrl}">Главная</a></li>
            <li><a href="{cfgSiteUrl}about">О портале</a></li>
            <li><a href="{cfgSiteUrl}employers">Работодателям</a></li>
            <li><a href="{cfgSiteUrl}applicants">Соискателям</a></li>
        </ul>
    </div>
    <div class="fr">
        <ul class="inline">
            <li><a href="{cfgSiteUrl}contacts">Контакты</a></li>
            <li><a href="{cfgSiteUrl}partner_to_be">Стать партнёром</a></li>
        </ul>
    </div>
</div>
