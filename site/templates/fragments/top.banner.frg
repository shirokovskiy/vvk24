<!-- TOP BANNER-->
<if show.top.banner>
    <div id="top-banner">
        {contentTopBanner}
    </div>
</if show.top.banner>
<!-- // TOP BANNER-->

<if show.top.banners>
    <ul id="top_banner" class="jcarousel-skin-banner">
        <loop top.banners>
            <li><a href="{strUrl}" target="_blank"><img src="{cfgAllImg}banners/{si_filename}" alt="{si_desc}" border="0"/></a></li>
        </loop top.banners>
    </ul>
</if show.top.banners>
