<div id="footer">
    <ul class="inline clearer">
        <li>
            <ul>
                <li><a href="{cfgSiteUrl}about">О портале</a></li>
                <li><a href="{cfgSiteUrl}news">Новости</a></li>
                <li><a href="{cfgSiteUrl}pravila_raboty_na_portale">Правила работы на портале</a></li>
                <li><a href="{cfgSiteUrl}reklama_na_portale">Реклама на портале</a></li>
                <li><a href="{cfgSiteUrl}contacts">Контактная информация</a></li>
                <li><a href="#uc" class="uc">Стандартный договор</a></li>
            </ul>
        </li>
        <li>
            <ul>
                <li><a href="{cfgSiteUrl}employers">Работодателям</a></li>
                <li><a href="{cfgSiteUrl}usloviya_razmesheniya_vacansij">Условия размещения вакансий</a></li>
                <li><a href="{cfgSiteUrl}sovety_dlya_rabotodateley">Советы для работодателей</a></li>
                <li><a href="{cfgSiteUrl}price-list">Прайс-лист</a></li>
                <li><a href="{cfgSiteUrl}vypisat_schet">Выписать счёт</a></li>
            </ul>
        </li>
        <li>
            <ul>
                <li><a href="{cfgSiteUrl}applicants">Соискателям</a></li>
                <li><a href="{cfgSiteUrl}usloviya_razmesheniya_resume">Условия размещения резюме</a></li>
                <li><a href="{cfgSiteUrl}sovety_dlya_soiskateley">Советы для соискателей</a></li>
            </ul>
        </li>
    </ul>

    <p id="copyright_notes">
        Внимание! При обращении в организацию, просьба ссылаться на сайт газеты "Все вакансии". При перепечатывании материалов, активная ссылка на сайт обязательна. &copy; Все права защищены. <br>
        Новая версия сайта находится в стадии бета-тестирования. Возможны проблемы в работе некоторых страниц. <br>
        Если Вы обнаружили ошибку или неточность, просим написать об этом в службу поддержки. <br>
        <a href="#uc" class="uc">Закон о СМИ</a>.
    </p>
</div>

<img src="{cfgSiteImg}ajax-circle.gif" width="200" alt="ajax-loader" id="ajax-loader"/>
