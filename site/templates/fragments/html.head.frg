<title>{m_title}</title>
<meta http-equiv="Content-Type" content="text/html; charset={METACHARSET}" />
<meta name="keywords" content="{m_keywords}" />
<meta name="description" content="{m_description}" />
<meta name="distribution" content="global" />
<meta name="robots" content="all" />
<meta name="author" content="Dimitry Shirokovskiy, phpwebstudio.com" />
<meta http-equiv="pragma" content="no-cache" />
<meta name="classification" content="Auto" />
<meta name="Document-state" content="Dynamic" />
<link rel="icon" href="{cfgSiteImg}favicon.ico?{rand}" type="image/x-icon" />
<link rel="shortcut icon" href="{cfgSiteImg}favicon.ico?{rand}" type="image/x-icon" />
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.js"></script>
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.mw.js"></script>
<script type="text/javascript" src="{cfgSiteUrl}ext/js/autocolumn.min.js"></script>
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.ui.js"></script>
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.ui.ru.js"></script>
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.jcarousel.js"></script>
<script type="text/javascript" src="{cfgSiteUrl}ext/js/gui.js"></script>
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.jqz.js"></script>
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.z.js"></script>

<link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/general.css" />
<link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/ui/custom.css" />
<link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/jq.tools.css" />
<link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/jq.fb.css" media="screen" />
<link rel="stylesheet" type="text/css" href="{cfgSiteUrl}ext/css/jq.jqz.css" />
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.fb.js"></script>
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.t.js"></script>

<!--[if lt IE 9]><script type="text/javascript" src="{cfgSiteUrl}ext/js/excanvas.js"></script><![endif]-->
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.tc.js"></script>
<script type="text/javascript" src="{cfgSiteUrl}ext/js/jq.mi.js"></script>

<script type="text/javascript" src="{cfgSiteUrl}ext/js/funcs.js"></script>
