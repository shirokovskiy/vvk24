<h2 class="title">Создать резюме</h2>

<nav class="center-align">
          <ul class="steps-resume">
              <li><a class="steps-resume-active" id="but-step1" href="#"><span class="big-letter">1</span>Основная информация</a></li>
              <li><a class="steps-resume" id="but-step2" href="#"><span class="big-letter">2</span>Пожелания к работе</a></li>
              <li><a class="steps-resume" id="but-step3" href="#"><span class="big-letter">3</span>Опыт <br/> работы</a></li>
              <li><a class="steps-resume" id="but-step4" href="#"><span class="big-letter">4</span>Образование</a></li>
              <li><a class="steps-resume" id="but-step5" href="#"><span class="big-letter">5</span>Дополнительная информация</a> </li>
          </ul>
      </nav>

<div class="clearer"></div>
<div class="page-note">
    <p class="font-small color-red">Внимание: поля, помеченные звездочкой (*), обязательны для заполнения!</p>
</div>

<form id="add_resume_form" class="create_form" method="post" enctype="multipart/form-data">
<input type="hidden" name="add" value="resume"/>
<div id="main-info" class="active-block">
    <h3>Личные данные</h3>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p><span class="after-star">Ф.И.О.</span></p></td>
            <td><input type="text" class="width450" required id="contact-fio" name="strFio" /></td>
        </tr>
        <tr>
            <td class="right-align"><p><span class="after-star">Пол</span></p></td>
            <td>
                <ul>
                    <li><input type="radio" id="rbSex2" name="rbSex" value="male" checked /> <label for="rbSex2">мужской</label></li>
                    <li><input type="radio" id="rbSex3" name="rbSex" value="female"/> <label for="rbSex3">женский</label></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td class="right-align"><p>Дата рождения</p></td>
            <td><p><input type="text" class="width100" id="contact-birth" name="strBirthday" size='10' maxlength="10" placeholder="ДД.ММ.ГГГГ"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Фотография</p></td>
            <td><img src="{cfgSiteImg}no-photo.gif" width="250px" height="320px" id="img-photo" />
                <button class="blue-button" type="button" id="btnUploadFile">Выбрать файл</button>
                <input type="file" class="width450" id="but-file"/>

                <p>*.jpg, *.png, *.tiff, *.bmp (не более 2MB). </p>
                <p>На фотографии должно быть четко видно лицо кандидата,снимок не должен содержать рекламы (например, логотипа соц.сети) и контактов соискателя. </p>
            </td>
        </tr>

        <tr>
            <td class="right-align"><p><span class="after-star">Город проживания</span></p></td>
            <td><p><input type="text" class="width450" name="strCity" id="contact-place-live"></p></td>
        </tr>
    </table>

    <h3>Контактные данные</h3>

    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p><span class="after-star">E-mail</span></p></td>
            <td><input type="text" class="width450" id="contact-mail" name="strEmail" maxlength="128"/></td>
        </tr>
        <tr>
            <td class="right-align"><p>Мобильный телефон</p></td>
            <td><p><input type="text" class="width60" placeholder="+7" maxlength="4" name="mobPhone[]" id="mobCCode" /> <input type="text" class="width60" placeholder="код" maxlength="4" name="mobPhone[]" id="mobECode"/> <input type="text" class="width100" placeholder="номер" maxlength="7" name="mobPhone[]" id="mobPhone"/> <input type="text" class="width140" placeholder="комментарий" maxlength="255" name="mobPhone[]"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Домашний телефон</p></td>
            <td><p><input type="text" class="width60" placeholder="+7" maxlength="4" name="homePhone[]" id="homeCCode"/> <input type="text" class="width60" placeholder="код" maxlength="4" name="homePhone[]" id="homeECode"/> <input type="text" class="width100" placeholder="номер" maxlength="7" name="homePhone[]" id="homePhone"/> <input type="text" class="width140" placeholder="комментарий" maxlength="255" name="homePhone[]"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Рабочий телефон</p></td>
            <td><p><input type="text" class="width60" placeholder="+7" maxlength="4" name="workPhone[]" id="workCCode"/> <input type="text" class="width60" placeholder="код" maxlength="4" name="workPhone[]" id="workECode"/> <input type="text" class="width100" placeholder="номер" maxlength="7" name="workPhone[]" id="workPhone"/> <input type="text" class="width140" placeholder="комментарий" maxlength="255" name="workPhone[]"/></p></td>
        </tr>
    </table>

    <br/><br/>

    <div class="center-align">
        <button class="create-vacancy" id="next-step1">Следующий шаг</button>
    </div>
    <br/>
</div>

<div id="wish-job-info" style="display: none">
    <h3>Пожелания к работе</h3>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p><span class="after-star">Должность</span></p></td>
            <td><input type="text" class="width450" id="contact-position" name="strTitle" maxlength="128"/></td>
        </tr>

        <tr>
            <td class="right-align"><p><span class="after-star">Профессиональная сфера</span></p></td>
            <td><p><input type="text" class="width450" id="contact-proff-area" name="proff"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p><span class="after-star">Заработная плата</span></p></td>
            <td><p><input type="text" class="width100" id="contact-salary" name="strSalary" value="30000" maxlength="7"/> руб./месяц</p></td>
        </tr>
        <tr>
            <td class="right-align"><p>График работы</p></td>
            <td>
                <p><input type="radio" name="choose-schedule-work" value="any" checked id="schedule-any"> Любой</p>
                <p><input type="radio" name="choose-schedule-work" value="ch" id="schedule-choose"/> Выбрать</p>
                <p class="padding-left-15"> <input type="checkbox" name="type-work[]" value="full" checked disabled/> полный рабочий день</p>
                <p class="padding-left-15"> <input type="checkbox" name="type-work[]" value="change" disabled/> сменный</p>
                <p class="padding-left-15"> <input type="checkbox" name="type-work[]" value="free" disabled/> свободный</p>
                <p class="padding-left-15"> <input type="checkbox" name="type-work[]" value="part" disabled/> частичная занятость</p>
                <p class="padding-left-15"> <input type="checkbox" name="type-work[]" value="remote" disabled/> удаленная работа</p>
            </td>
        </tr>

        <tr>
            <td class="right-align"><p><span class="after-star">Место работы (город)</span></p></td>
            <td><p><input type="text" class="width450" name="strExpCity" id="contact-job-place"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Командировки</p></td>
            <td>
                <p><input type="radio" name="rbTrips" value="Y" checked> Готов к командировкам</p>
                <p><input type="radio" name="rbTrips" value="N" > Не готов к командировкам</p>
            </td>
        </tr>
    </table>
    <br/><br/>
    <div class="center-align">
        <!--<a href="#"> Предыдущий шаг</a> -->
        <button class="create-vacancy" id="prev-step2">Предыдущий шаг</button>
        <button class="create-vacancy" id="next-step2">Следующий шаг</button>
    </div>
    <br/>
</div>

<div id="experience-info" style="display: none">
    <h3>Работа в должности</h3>
    <table class="table-fixed-col border-bottom1">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p><span class="after-star">Опыт работы в данной должности</span></p></td>
            <td><p><input type="text" class="width100" name="intExp" id="intExp" value="1"/>лет</p>
                <p><input type="checkbox" name="no-experience" id='no-experience' value="yes"/> нет опыта работы</p></td>
        </tr>
    </table>

    <h3>Опыт работы</h3>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p>Название компании</p></td>
            <td><p><input type="text" class="width450" id="contact-comp-name" name="strCompany" maxlength="128"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Должность</p></td>
            <td><p><input type="text" class="width450" id="contact-position-now" name="strPosition" maxlength="128"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Период работы</p></td>
            <td><p>с <input type="text" class="width60" name="strDateFrom" id="job-from"> по <input type="text" class="width60" name="strDateTo" id="job-till"></p>
                <p class="under-text color-gray font-small">Если Вы еще работаете в данной организации, поле "по" заполнять не нужно</p>
            </td>
        </tr>
        <tr>
            <td class="right-align"><p>Описание работы</p></td>
            <td><textarea type="text" class="width450" id="contact-about-job" name="strResponsibility"></textarea></td>
        </tr>
    </table>

    <br/><br/>
    <div class="center-align">
        <!--<a href="#"> Предыдущий шаг</a> -->
        <button class="create-vacancy" id="prev-step3">Предыдущий шаг</button>
        <button class="create-vacancy" id="next-step3">Следующий шаг</button>
    </div>
    <br/>
</div>


<div id="education-info" style="display: none">
    <h3>Образование</h3>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p><span class="after-star">Основное</span></p></td>
            <td><p><input type="radio" name="education-type" value="fh" checked> высшее</p>
                <p><input type="radio" name="education-type" value="ph" > неполное высшее</p>
                <p><input type="radio" name="education-type" value="ms" > среднее специальное</p>
                <p><input type="radio" name="education-type" value="m" > среднее</p>
            </td>
        </tr>
        <tr>
            <td class="right-align"><p>Название учебного заведения</p></td>
            <td><p><input type="text" class="width450" id="contact-school" maxlength="255" name="strInstitution"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p><span class="after-star">Полученное образование</span></p></td>
            <td><p><input type="text" class="width450" name="education" id="contact-education" maxlength="255"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p><span class="after-star">Годы обучения</span></p></td>
            <td><p>с <input type="text" class="width60" name="strEduDateStart" id="contact-school-from" maxlength="4"/> по <input type="text" class="width60" name="strEduDateFin" id="contact-school-till" maxlength="4"/></p>
                <p class="under-text color-gray font-small">Если Вы еще учитесь в данном учебном заведении, поле "по" заполнять не нужно</p>
            </td>
        </tr>
        <tr>
            <td class="right-align"><p>Специальность</p></td>
            <td><p><input type="text" class="width450" name="strSpecialization" id="contact-speciality"></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Комментарий</p></td>
            <td><textarea type="text" class="width450" name="strEduPlus"></textarea><br/>
                <p class="under-text color-gray font-small">Награды, темы диплома, средний балл и т.п.</p>
            </td>
        </tr>
    </table>

    <br/><br/>

    <div class="center-align">
        <!--<a href="#"> Предыдущий шаг</a> -->
        <button class="create-vacancy" id="prev-step4">Предыдущий шаг</button>
        <button class="create-vacancy" id="next-step4">Следующий шаг</button>
    </div>
    <br/>
</div>


<div id="additional-info" style="display: none">
    <h3>Дополнительная информация</h3>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td class="right-align"><p>Родной язык</p></td>
            <td><p><input name="strLang" id="strLang" type="text" class="width120" placeholder="Русский" maxlength="32"/></p></td>
        </tr>
        <tr>
            <td class="right-align"><p>Другие языки</p></td>
            <td><p><input type="checkbox" name="no-lang" value="no"/> не владею другими языками</p>

                <div id="lang-box">
                    <p><a href="#" id="but-add-lang">добавить язык</a></p>
                    <p><input type="text" class="width120" name="moreLang[]" placeholder="укажите язык" maxlength="32"/><input type="text" class="width120" placeholder="уровень владения" maxlength="32" name="moreLangLevel[]"/></p>
                </div>
            </td>
        </tr>
        <tr>
            <td class="right-align"><p>Профессиональные навыки</p></td>
            <td><textarea type="text" class="width450" name="experience_description"></textarea>
                <p class="under-text color-gray font-small">Опишите свои знания и навыки, которые соответствуют выбранной Вами должности</p></td>
        </tr>
        <tr>
            <td class="right-align"><p>О себе</p></td>
            <td><textarea type="text" class="width450" name="strAbout"></textarea>
                <p class="under-text color-gray font-small">Укажите информацию о себе, которая может заинтересовать потенциального работодателя</p></td>
        </tr>

        <tr>
            <td class="right-align"><p>Гражданство</p></td>
            <td><p><input type="text" class="width450" maxlength="64" name="citizenship"/></p></td>
        </tr>
        <!--<tr>-->
            <!--<td class="right-align"><p> </p></td>-->
            <!--<td><p><a href="#"> Добавить портфолио</a></p></td>-->
        <!--</tr>-->
    </table>

    <br/><br/>

    <div class="center-align">
        <!--<a href="#"> Предыдущий шаг</a> -->
        <button class="create-vacancy" id="prev-step5">Предыдущий шаг</button>
        <button class="create-vacancy" id="add-resume" type="submit">Разместить резюме</button>
    </div>
    <br/>
</div>
</form>

<script type="text/javascript">
jQuery(document).ready(function ($) {
    $('#contact-fio').focus();
    $('#contact-salary').spinner({
        min: 0,
        step: 5000
    });
    $('a.ui-spinner-button').css('border','none');
    $('#contact-birth').datepicker({
        showOn: "button",
        buttonImage: "{cfgSiteImg}calendar_icon.png",
        buttonImageOnly: true,
        dateFormat: "dd.mm.yy",
        changeYear: true,
        showAnim: 'fold',
        defaultDate: '-30Y'
    });

    $('#contact-birth').blur(function(){
        if ($('#contact-birth').val().length>0) {
            var rBirth=/^(0[1-9]|[12][0-9]|3[01])[- /.](0[1-9]|1[012])[- /.](19|20)\d\d$/;
            if (!rBirth.test($('#contact-birth').val()))
            {
                alert('Вы ввели не корректную дату рождения!(Формат даты: DD.MM.YYYY)');
                $('#contact-birth').focus();
            }
        }
        return false;
    });
    $('#contact-mail').blur(function(){
        if ($('#contact-mail').val().length>0) {
            var rMail=/^[-\w.]+@([A-z0-9][-A-z0-9]+\.)+[A-z]{2,4}$/;
            if (!rMail.test($('#contact-mail').val()))
            {
                alert('Введите корректный e-mail!');
                $('#contact-mail').focus();
            }
        }
        return false;
    });
    var citiesSource = [{strCities}];
    $('#contact-place-live').autocomplete({source: citiesSource, minLength: 3});
    $('#contact-job-place').autocomplete({source: citiesSource, minLength: 3});
    var arrLangs = ['Русский','Английский','Французкий','Японский','Итальянский','Испанский','Немецкий'];
    $('#strLang').autocomplete({source: arrLangs, minLength: 1});

    $('#but-step1').click(function(){
        $('.active-block').hide().removeClass('active-block');
        $('#main-info').show().addClass('active-block');
        $('.steps-resume-active').removeClass('steps-resume-active');
        $('#but-step1').addClass('steps-resume-active');
        return false;
    });

    $('#but-step2').click(function(){
        if( isStep1OK() )
        {
            $('.active-block').hide().removeClass('active-block');
            $('#wish-job-info').show().addClass('active-block');
            $('.steps-resume-active').removeClass('steps-resume-active');
            $('#but-step2').addClass('steps-resume-active');
        }
        else alert('Заполните обязательные поля!');
        $('#contact-position').focus();
        return false;
    });

    $('#but-step3').click(function(){
        if( isStep1OK() && isStep2OK() )
        {
            $('.active-block').hide().removeClass('active-block');
            $('#experience-info').show().addClass('active-block');
            $('.steps-resume-active').removeClass('steps-resume-active');
            $('#but-step3').addClass('steps-resume-active');
        }
        else alert('Заполните обязательные поля!');
        return false;
    });
    $('#but-step4').click(function(){
        if ( isStep1OK() && isStep2OK() && isStep3OK() )
        {
            $('.active-block').hide().removeClass('active-block');
            $('#education-info').show().addClass('active-block');
            $('.steps-resume-active').removeClass('steps-resume-active');
            $('#but-step4').addClass('steps-resume-active');
        }
        else alert('Заполните обязательные поля!');
        $('#contact-school').focus();
        return false;
    });
    $('#but-step5').click(function(){
        if ( isStep1OK() && isStep2OK() && isStep3OK() )
        {
            $('.active-block').hide().removeClass('active-block');
            $('#additional-info').show().addClass('active-block');
            $('.steps-resume-active').removeClass('steps-resume-active');
            $('#but-step5').addClass('steps-resume-active');
        }
        else alert('Заполните обязательные поля!');
        return false;
    });

    $('#next-step1').click( function(){
        $('#but-step2').trigger('click');
        return false;
    });

    $('#next-step2').click(function(){
        $('#but-step3').trigger('click');
        return false;
    });

    $('#prev-step2').click(function(){
        $('.active-block').hide().removeClass('active-block');
        $('#main-info').show().addClass('active-block');
        $('.steps-resume-active').removeClass('steps-resume-active');
        $('#but-step1').addClass('steps-resume-active');
        return false;
    });

    $('#next-step3').click(function(){
        $('#but-step4').trigger('click');
        return false;
    });
    $('#prev-step3').click(function(){
        $('.active-block').hide().removeClass('active-block');
        $('#wish-job-info').show().addClass('active-block');
        $('.steps-resume-active').removeClass('steps-resume-active');
        $('#but-step2').addClass('steps-resume-active');
        return false;
    });

    $('#next-step4').click(function(){
        $('#but-step5').trigger('click');
        return false;
    });
    $('#prev-step4').click(function(){
        $('.active-block').hide();
        $('#experience-info').show();
        $('.steps-resume-active').removeClass('steps-resume-active');
        $('#but-step3').addClass('steps-resume-active');
        $('.active-block').removeClass('active-block');
        $('#experience-info').addClass('active-block');
        return false;
    });

    $('#prev-step5').click(function(){
        $('.active-block').hide();
        $('#education-info').show();
        $('.steps-resume-active').removeClass('steps-resume-active');
        $('#but-step4').addClass('steps-resume-active');
        $('.active-block').removeClass('active-block');
        $('#education-info').addClass('active-block');
        return false;
    });


    function isStep1OK () {
        return ( $('#contact-fio').val().length>0
                && $('#contact-place-live').val().length>0
                && $('#contact-mail').val().length>0
                && $('input[name="rbSex"]').is(':checked')
                &&  (   ($('#mobCCode').val().length>0 && $('#mobECode').val().length>0 && $('#mobPhone').val().length>0)
                    ||  ($('#homeCCode').val().length>0 && $('#homeECode').val().length>0 && $('#homePhone').val().length>0)
                    ||  ($('#workCCode').val().length>0 && $('#workECode').val().length>0 && $('#workPhone').val().length>0)
                    )
        );
    }

    function isStep2OK () {
        return ( $('#contact-position').val().length>0
                //&& $('#intCategoryProffId').val().length>0
                && $('#contact-proff-area').val().length>0
                && $('#contact-salary').val().length>0
                && $('#contact-job-place').val().length>0
        );
    }

    function isStep3OK () {
        return ($('#no-experience').is(':checked') || $('#intExp').val().length>0);
    }

    $('#schedule-choose').click(function(){
             $('.padding-left-15 input').removeAttr('disabled');
    });
    $('#schedule-any').click(function(){
             $('.padding-left-15 input').attr('disabled', 'disabled');
    });

    $('#but-file').change(function(){
        var str = $('#but-file').get(0).files[0].name;
        $('#img-photo').attr('src',$('#but-file').val())
    });
    $('#btnUploadFile').click(function(){
            $('#but-file').trigger('click');
             $('#but-file').trgger('change')
        });

    $('#but-add-lang').click(function(){
        var str='<p class="margin5-0"><input type="text" class="width120" placeholder="укажите язык"><input type="text" class="width120" placeholder="уровень владения"></p>';
        $('#lang-box').append(str);
        return false;
    });
    $('input[name="no-lang"]').change(function(){
        if ($(this).is(":checked"))
            $('#lang-box').hide('medium');
        else
            $('#lang-box').show('medium');
    });

    $('#contact-salary').input_mask("9?999999", {placeholder:" "});
    $('#intExp').input_mask("9?9", {placeholder:" "});
    $('#contact-school-from').input_mask("9999");
    $('#contact-school-till').input_mask("9999");
    $('#job-from').input_mask("9999");
    $('#job-till').input_mask("9999");

    $('#add-resume').click(function () {
            console.log('Prevent default');
            event.preventDefault();

            if (isStep1OK() && isStep2OK() && isStep3OK()) {
                $('#add_resume_form').submit();
            }

            return false;
    });

<if is.msg>
    var status = {msgStatus};
    if (status>0) {
        $('#add_resume_msg').removeClass().addClass('err');
    } else {
        $('#add_resume_msg').removeClass().addClass('success');
    }
    $.fancybox('#add_resume_msg');
</if is.msg>
});
</script>


<div id="add_resume_msg" class="warn">{strMessage}</div>
