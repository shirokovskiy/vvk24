<div class="inner_page">
    <h4 class="title"><a href="#uc" class="uc">Работодатели</a> / <span>{strCompanyName}</span></h4>
    <if is.strLogo><div class="cell-logo"><img src="{cfgAllImg}companies/{strLogo}" alt="{strCompanyName}"/></div></if is.strLogo>

    <table class="record-card company-info clearer">
        <colgroup>
            <col class="first">
            <col>
        </colgroup>

        <tr>
            <td colspan="2"><b>О компании</b></td>
        </tr>

        <if is.strCity>
            <tr>
                <td>Город</td>
                <td>{strCity}</td>
            </tr>
        </if is.strCity>
        <if is.strAddress>
            <tr>
                <td>Адрес</td>
                <td>{strAddress}</td>
            </tr>
        </if is.strAddress>
        <if is.strWeb>
            <tr>
                <td>Web-site</td>
                <td>{strWeb}</td>
            </tr>
        </if is.strWeb>
        <if is.strEmail>
            <tr>
                <td>Email</td>
                <td>{strEmail}</td>
            </tr>
        </if is.strEmail>
        <if is.strPhone>
            <tr>
                <td>Телефон</td>
                <td>{strPhone}</td>
            </tr>
        </if is.strPhone>
        <if is.strDescription>
            <tr>
                <td>Описание</td>
                <td>{strDescription}</td>
            </tr>
        </if is.strDescription>

        <tr>
            <td colspan="2">
                <if is.list>
                <h4 class="company-info">Вакансии компании:</h4>
                <div class="vacancies list">
                    <loop list.vacancies>
                    <div class="row">
                        <span class="strS">Вакансия №{jv_id}, {strDatePublish}, {strTimePublish}, {jv_city}</span><br/>
                        <a hrefa="{cfgSiteUrl}vacancy/{jv_id}" href="#uc" class="uc">{jv_title}</a>
                        <p>{strDescription} <if is.strDescription.{jv_id}><a href="{cfgSiteUrl}vacancy/{jv_id}">&raquo;</a></if is.strDescription.{jv_id}></p>
                        <span class="salary">{strSalary}</span> <br/>
                        <div>{strContactInfo}</div>
                    </div>
                    </loop list.vacancies>
                </div>
                </if is.list>
            </td>
        </tr>
    </table>
</div>
