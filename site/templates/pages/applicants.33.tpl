<div class="layer_light_blue page_employers">
	<div>
		<h4>Используйте все возможности!</h4>
		<p>Прямо сейчас вы можете рассказать о себе.</p>
		<p>Ваши контакты открыты всем работодателям.</p>
		<p>Они готовы биться за вас и именно вас заполучить к себе в команду!</p>
	</div>

	<div class="button_field">
		<a href="{cfgSiteUrl}add-resume"><button id="add_vacancy_btn"><span>Разместить резюме</span></button></a>
	</div>
</div>
