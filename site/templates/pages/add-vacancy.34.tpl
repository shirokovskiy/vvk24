<h2 class="title">Разместить вакансию</h2>

<div class="page-note">
    <p>Внимание: поля, помеченные звездочкой (*), обязательны для заполнения!</p>
</div>
<form id="add_vacancy_form" class="create_form" method="post" enctype="multipart/form-data">
    <input type="hidden" name="add" value="vacancy"/>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td><p><span class="after-star">Город</span></p></td>
            <td><input type="text" class="width450" id="strCity" name="strCity" required /></td>
        </tr>
        <tr>
            <td><p><span class="after-star">Должность</span></p></td>
            <td><input type="text" class="width450" name="strTitle" required /></td>
        </tr>
        <tr>
            <td><p>Оклад</p></td>
            <td><p>от <input type="text" class="width100" name="strSalaryFrom" id="strSalaryFrom" maxlength="7"/> до <input type="text" class="width100" name="strSalaryTo" id="strSalaryTo" maxlength="7" /></p></td>
        </tr>
        <tr>
            <td><p>Возраст</p></td>
            <td><p>от <input type="text" class="width100" name="strAgeFrom" id="strAgeFrom" maxlength="2"/> до <input type="text" class="width100" name="strAgeTo" id="strAgeTo" maxlength="2"/></p></td>
        </tr>
        <tr>
            <td><p>Пол</p></td>
            <td>
                <ul>
                    <li><input type="radio" id="rbSex1" name="rbSex" value="" checked /> <label for="rbSex1">не важно</label></li>
                    <li><input type="radio" id="rbSex2" name="rbSex" value="male"/> <label for="rbSex2">мужской</label></li>
                    <li><input type="radio" id="rbSex3" name="rbSex" value="female"/> <label for="rbSex3">женский</label></li>
                </ul>
            </td>
        </tr>
        <tr>
            <td><p>Опыт работы</p></td>
            <td><input type="text" name="strExp" class="width450" /></td>
        </tr>
        <tr>
            <td><p>Дополнительная информация</p></td>
            <td><textarea type="text" name="strDescription" class="width450"></textarea></td>
        </tr>
    </table>

    <p class="header-form-create">Контактные данные работодателя</p>
    <table class="table-fixed-col">
        <col class="col1">
        <col class="col2">
        <tr>
            <td><p><span class="after-star">Ф.И.О.</span></p></td>
            <td><input type="text" name="strContactPerson" class="width450" required /></td>
        </tr>
        <tr>
            <td><p><span class="after-star">Название компании</span></p></td>
            <td><input type="text" name="strCompany" class="width450" required /></td>
        </tr>
        <tr>
            <td><p>Должность</p></td>
            <td><input type="text" name="strPosition" class="width450"></td>
        </tr>
        <tr>
            <td><p><span class="after-star">E-mail</span></p></td>
            <td><input type="text" name="strCompanyEmail" class="width450" required maxlength="255" /></td>
        </tr>
        <tr>
            <td><p><span class="after-star">Телефон</span></p></td>
            <td>
                <table>
                    <tr>
                        <td><input type="text" name="phone[]" class="width60" id="telCode" maxlength="5"></td>
                        <td><input type="text" name="phone[]" class="width100" id="telNum" required maxlength="7" /></td>
                        <td><input type="text" name="phone[]" class="width60" id="telExt" maxlength="5"></td>
                    </tr>
                    <tr>
                        <td align="center">код</td>
                        <td align="center">телефон</td>
                        <td align="center">доб.</td>
                    </tr>
                </table>
            </td>
        </tr>

    </table>
    <button class="create-vacancy">Создать</button>
</form>

<if is.msg>
<script type="text/javascript">
    $(function(){
        var status = {msgStatus};
        if (status>0) {
            $('#add_vacancy_msg').removeClass().addClass('err');
        } else {
            $('#add_vacancy_msg').removeClass().addClass('success');
        }
        $.fancybox('#add_vacancy_msg');
    });
</script>
</if is.msg>

<div id="add_vacancy_msg" class="warn">{strMessage}</div>

<script type="text/javascript">
    var citiesSource = [{strCities}];

    jQuery(document).ready(function ($) {
        $('#strCity').autocomplete({source: citiesSource, minLength: 3});
        $('#strAgeFrom').input_mask("99");
        $('#strAgeTo').input_mask("99");
        //$('#strSalaryFrom').input_mask("9999999");
        //$('#strSalaryTo').input_mask("9999999");
    });
</script>
