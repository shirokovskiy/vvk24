<h3>Вакансия: {strTitle} (<span>ID: {intVacancyId}</span>)</h3>

<table class="record-card">
    <colgroup>
        <col class="first">
        <col>
    </colgroup>

    <if is.strSalary>
        <tr>
            <td>Предлагаемая з/п</td>
            <td class="salary">{strSalary}</td>
        </tr>
    </if is.strSalary>

    <if is.strCompany>
        <tr>
            <td>Компания</td>
            <td>{strCompany}</td>
        </tr>
    </if is.strCompany>

    <tr>
        <td>Город</td>
        <td>{strCity}</td>
    </tr>

    <if is.strSex>
        <tr>
            <td>Пол кандидата</td>
            <td>{strSex}</td>
        </tr>
    </if is.strSex>

    <if is.strEdu>
        <tr>
            <td>Требуемое образование</td>
            <td>{strEdu}</td>
        </tr>
    </if is.strEdu>

    <if is.strWorkbusy>
        <tr>
            <td>Предстоящая занятость</td>
            <td>{strWorkbusy}</td>
        </tr>
    </if is.strWorkbusy>

    <if is.strDriver>
        <tr>
            <td>Наличие водительского удостоверения</td>
            <td>{strDriver}</td>
        </tr>
    </if is.strDriver>

    <if is.strTrips>
        <tr>
            <td>Готовность к командировкам</td>
            <td>{strTrips}</td>
        </tr>
    </if is.strTrips>

    <if is.strFio>
        <tr>
            <td>Контактное лицо</td>
            <td>{strFio}</td>
        </tr>
    </if is.strFio>


    <if is.strPhone>
        <tr>
            <td>Телефон</td>
            <td>{strPhone}</td>
        </tr>
    </if is.strPhone>

    <if is.strEmail>
        <tr>
            <td>Email</td>
            <td>{strEmail}</td>
        </tr>
    </if is.strEmail>

    <if is.strDescription>
        <tr>
            <td>Дополнительная информация</td>
            <td>{strDescription}</td>
        </tr>
    </if is.strDescription>

    <tr>
        <td>Дата публикации</td>
        <td>{strDatePublish}</td>
    </tr>
</table>
