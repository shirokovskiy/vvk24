<div id="cloudTags" class="block">
    <!--Ведущие компании-->
    <ul id="company_tags" class="jcarousel jcarousel-skin-brands">
        <loop list.top.companies>
            <li><a href="{cfgSiteUrl}company-vacancies/{jc_id}"><img class="brand" src="{cfgAllImg}companies/{si_filename}" alt="{jc_title}"/></a></li>
        </loop list.top.companies>
    </ul>
</div>
<frg top.banner>
<div id="search_box" class="clearer">
    <ul class="inline">
        <li>
            <h1>Вакансии работодателей</h1>

            <form action="{cfgSiteUrl}search/vacancy" method="post">
	            <input type="hidden" id="type" name="type" value="vacancies"/>
                <div class="search_block">
                    <p class="label">Например: <span>бухгалтер</span></p>
                    <div><input type="text" id="strSearchVac" name="query" value="" class="field" maxlength="255"/></div>
                    <p><a href="#uc" class="uc dropdown">Профессиональная область</a></p>
                    <p class="label">Зарплата от:</p>
                    <div><input type="text" id="strSearchVacSal" name="search_salary" value="" class="field" maxlength="255"/></div>
                    <input type="submit" name="submit" value="Найти вакансию" class="btn_red" />
                </div>
            </form>
        </li>
        <li>
            <h1>Резюме соискателей</h1>

            <form action="{cfgSiteUrl}search/resume" method="post">
	            <input type="hidden" id="type" name="type" value="resumes"/>
                <div class="search_block">
                    <p class="label">Например: <span>бухгалтер</span></p>
                    <div><input type="text" id="strSearchVac" name="query" value="" class="field" maxlength="255"/></div>
                    <p><a href="#uc" class="uc dropdown">Профессиональная область</a></p>
                    <p class="label">Зарплата до:</p>
                    <div><input type="text" id="strSearchVacSal" name="search_salary" value="" class="field" maxlength="255"/></div>
                    <input type="submit" name="submit" value="Найти сотрудника" class="btn_blue" />
                </div>
            </form>
        </li>
    </ul>
</div>
<div id="second_navigation">
    <ul class="inline">
        <li><a href="{cfgSiteUrl}add-vacancy">Добавить вакансию</a></li>
        <li><a href="{cfgSiteUrl}add-resume">Добавить резюме</a></li>
    </ul>
</div>
<div id="content_box">
    <div id="two-box" class="clearer">
        <ul class="inline">
            <li>
                <div class="block_list">
                    <h2>Новые вакансии</h2>
                    <ul id="home_list_vacancies" class="jcarousel jcarousel-skin-vvk">
                    <loop list.top.vacancies>
                        <li>
                            <div class="row item">
                                <a href="{cfgSiteUrl}vacancy/{jv_id}">{jv_title}</a><br />
                                {strSalary}<br />
                                Город: {jv_city}<br />
                                Тел: {strPhone}<br />
                                Email: {strEmail}<br />
                            </div>
                        </li>
                    </loop list.top.vacancies>
                    <if list.top.vacancies>
	                    <li><p class="color-red">нет данных с учётом выбранного города: {strMyCity}</p></li>
                    </if list.top.vacancies>
                    </ul>
                </div>
            </li>
            <li>
                <div class="block_list">
                    <h2>Новые резюме</h2>
                    <ul id="home_list_resumes" class="jcarousel jcarousel-skin-vvk">
                    <loop list.top.resumes>
                        <li>
                            <div class="row item">
                                <a href="{cfgSiteUrl}resume/{jr_id}">{jr_title}</a><br />
                                {strSalary}<br />
                                Тел: {strPhone}<br />
                                Email: {strEmail}<br />
                            </div>
                        </li>
                    </loop list.top.resumes>
                    <if list.top.resumes>
						<li><p class="color-red">нет данных с учётом выбранного города: {strMyCity}</p></li>
                    </if list.top.resumes>
                    </ul>
                </div>
            </li>
        </ul>
    </div>
</div>

<script type="text/javascript">
jQuery(document).ready(function ($) {
    $("#home_list_vacancies").jcarousel({
        vertical: true
        , scroll: 1
        , auto: 2
        , wrap: 'last'
        , initCallback: mycarousel_initCallback
    });

    $("#home_list_resumes").jcarousel({
        vertical: true
        , scroll: 1
        , auto: 2
        , wrap: 'last'
        , initCallback: mycarousel_initCallback
    });

    $("#company_tags").jcarousel({
        auto: 10,
        scroll: 6,
        wrap: 'last',
        initCallback: mycarousel_initCallback
    });
});

/**
 * For the JCarousel
 * @param carousel
 */
function mycarousel_initCallback(carousel)
{
    // Disable autoscrolling if the user clicks the prev or next button.
    carousel.buttonNext.bind('click', function() {
        carousel.startAuto(0);
    });

    carousel.buttonPrev.bind('click', function() {
        carousel.startAuto(0);
    });

    // Pause autoscrolling if the user moves with the cursor over the clip.
    carousel.clip.hover(function() {
        //carousel.stopAuto();
    }, function() {
        carousel.startAuto();
    });
}
</script>
