<a name="new"></a>
<div class="page">
	<h3 class="centered"><span>Регистрация на сайте</span></h3>

	<ul class="big_list">
		<li>- <a href="javascript:void(0)" id="fizlitso">Я соискатель, ищу работу</a></li>
		<li>- <a href="javascript:void(0)" id="jurlitso">Я представитель компании, ищу сотрудника, или провожу тренинги</a></li>
	</ul>

	<div id="fizlitso_box" class="reg_form">
		<form id="fizlitso_form" action="/lib/api/registration.php">
			<input type="hidden" name="strType" value="fiz"/>
			<div class="clearer">
				<div class="fl col1">Email <span class="redstar">*</span></div>
				<div class="fl ui-widget"><input type="text" id="fiz_strEmail" name="strEmail" class="plane" maxlength="255" placeholder="ваш@email"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Придумайте пароль <span class="redstar">*</span></div>
				<div class="fl ui-widget"><input type="text" id="fiz_strPassword" name="strPassword" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Подтвердите пароль <span class="redstar">*</span></div>
				<div class="fl ui-widget"><input type="text" id="fiz_strPasswordRepeat" name="strPasswordRepeat" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Фамилия</div>
				<div class="fl ui-widget"><input type="text" id="fiz_strLname" name="strLname" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Имя</div>
				<div class="fl ui-widget"><input type="text" id="fiz_strFname" name="strFname" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Отчество</div>
				<div class="fl ui-widget"><input type="text" id="fiz_strMname" name="strMname" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Телефон</div>
				<div class="fl ui-widget"><input type="text" id="fiz_strPhone" name="strPhone" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1"></div>
				<div class="fl ui-widget"><input type="submit" id="fiz_submit" name="submit" value="Зарегистрироваться" class="button" /></div>
			</div>
		</form>

		<div class="block_a ac"><a href="javascript:void(0)">Назад</a></div>
	</div>
	<div id="jurlitso_box" class="reg_form">
		<form id="jurlitso_form" action="/lib/api/registration.php">
			<input type="hidden" name="strType" value="jur"/>
			<div class="clearer">
				<div class="fl col1">Email <span class="redstar">*</span></div>
				<div class="fl ui-widget"><input type="text" id="jur_strEmail" name="strEmail" class="plane" maxlength="255" placeholder="ваш@email"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Придумайте пароль <span class="redstar">*</span></div>
				<div class="fl ui-widget"><input type="text" id="jur_strPassword" name="strPassword" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Подтвердите пароль <span class="redstar">*</span></div>
				<div class="fl ui-widget"><input type="text" id="jur_strPasswordRepeat" name="strPasswordRepeat" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Фамилия</div>
				<div class="fl ui-widget"><input type="text" id="jur_strLname" name="strLname" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Имя</div>
				<div class="fl ui-widget"><input type="text" id="jur_strFname" name="strFname" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Отчество</div>
				<div class="fl ui-widget"><input type="text" id="jur_strMname" name="strMname" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Должность</div>
				<div class="fl ui-widget"><input type="text" id="jur_strPosition" name="strPosition" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">Телефон</div>
				<div class="fl ui-widget"><input type="text" id="jur_strPhone" name="strPhone" class="plane" maxlength="255"/></div>
			</div>
			<div class="clearer">
				<div class="fl col1">ИНН компании</div>
				<div class="fl ui-widget"><input type="text" id="jur_strInn" name="strInn" class="plane" maxlength="255"/></div>
			</div>
			<!--<div class="clearer">
	            <div class="fl col1"></div>
	            <div class="fl ui-widget"><img id="captcha" src="{secureImage}" alt="CAPTCHA Image" /></div>
	        </div>-->

			<div class="button_field">
				<button><span>Зарегистрироваться</span></button>
			</div>
			<div class="agreement">
				<input type="checkbox" id="jur_cbxAgree" name="cbxAgree" value="Y" class="plane" disabled />
				<label for="jur_cbxAgree"> Я прочитал(а) и полностью согласен(на) с <a href="#uc" class="uc">Пользовательским соглашением</a></label>
			</div>
		</form>

		<div class="block_a ac"><a href="javascript:void(0)">Назад</a></div>
	</div>
</div>


<script type="text/javascript">
	//<![CDATA[
	jQuery(function(){
		$('#fizlitso').click(function(){
			$('.big_list').toggle();
			$('#fizlitso_box').toggle();
		});
		$('#jurlitso').click(function(){
			$('.big_list').toggle();
			$('#jurlitso_box').toggle();
		});

		/**
		 * Нажата кнопка регистрации
		 */
		$('#fizlitso_form #submit').click(function(){
			alert('Test-1');
		});

		/*$('#jurlitso_form #submit').click(function(){
		 sendForm('#jurlitso_form', '/lib/api/registration.php');
		 });*/

		/* attach a submit handler to the form */
		$(".reg_form form").submit(function(event) {
			/* stop form from submitting normally */
			event.preventDefault();

			/* get some values from elements on the page: */
			var $form = $( this ),
					url = $form.attr( 'action'),
					validation = true,
					type = $form.find('input[name=strType]').val(),
					strEmail = $( '#'+type+'_strEmail' ).val();

			if (isEmpty(strEmail)) {
				validation = false;
				$.fancybox('Ошибка: Вы не заполнили поле Email.');
				$( '#'+type+'_strEmail').addClass('err').change(function(){
					$(this).removeClass('err');
				});
				return false;
			}

			var strPassword = $( '#'+type+'_strPassword' ).val();
			var strPasswordRepeat = $( '#'+type+'_strPasswordRepeat' ).val();

			if ((strPasswordRepeat != strPassword && !isEmpty(strPassword)) || isEmpty(strPasswordRepeat) || isEmpty(strPassword)) {
				// Если пароли не совпадают
				validation = false;
				$.fancybox('Ошибка: Вы неверно заполнили поля для паролей. Пароль должен быть введён дважды и одинаково.');
				$( '#'+type+'_strPassword').addClass('err').change(function(){
					$(this).removeClass('err');
					$('#'+type+'_strPasswordRepeat').removeClass('err');
				});
				$( '#'+type+'_strPasswordRepeat').addClass('err').change(function(){
					$(this).removeClass('err');
					$('#'+type+'_strPassword').removeClass('err');
				});
				return false;
			}

			if (validation) {
				/* Send the data using post */
				var posting = $.post( url, $form.serialize(), null, 'json' );

				/* Put the results in a div */
				posting.done(function( data ) {
					if (data.status == 1) {
						console.log('OK');
						$form.find("input[type=text]").val('');
						$('.big_list').toggle();
						$form.parent().toggle();

						// todo: перебросить человека в Личный кабинет (автоматическая авторизация)
					} else {
						console.log('NO RESULT. ERRORS!');
					}

					$.fancybox(data.message);
				});
			}
		});

		/**
		 * Нажата кнопка Назад
		 */
		$('.reg_form .block_a>a').click(function(){
			$('.big_list').toggle();
			$(this).parent().parent().toggle();
		});
	});
	//]]>
</script>


