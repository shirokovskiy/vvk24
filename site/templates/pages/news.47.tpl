<if news.list>
<h3>Все новости</h3>
<div class="cnt">{blockPaginator}</div>
<div class="news-list">
<loop last.news>
    <div class="news-cell clearer">
        <div class="news-box">
            <div class="news-text">
                <p class="txt-block"><a href="{cfgSiteUrl}news/{sn_id}" class="link-blue">{sn_title}</a></p>
            </div>
            <if news.picture.{sn_id}>
            <div class="fr news-image"><a href="{cfgAllImg}news/news.photo.{sn_id}.b.jpg{strUniquePhoto}" id="hrefImg{sn_id}"><img src="{cfgAllImg}news/news.photo.{sn_id}.jpg{strUniquePhoto}" border="0" width="200" /></a></div>
            </if news.picture.{sn_id}>
        </div>
    </div>
</loop last.news>
</div>
<div class="cnt">{blockPaginator}</div>
</if news.list>

<if novelty>
<h3>{sn_title}</h3>
<script type="text/javascript">
    $(document).ready(function() {
        <if picture>
            $("a#pictureMain").fancybox({
                'titleShow'		: true
                , 'titlePosition'	: 'over'
            });
        </if picture>
    });
</script>
<div class="cnt">
    <if picture>
        <a href="{cfgAllImg}news/news.photo.{sn_id}.b.jpg{strUniqueMainPhoto}" id="pictureMain"><img src="{cfgAllImg}news/news.photo.{sn_id}.jpg{strUniqueMainPhoto}" width="200" border="0" align="right" style="margin:0 0 5px 10px" alt="{strImageTitle}"></a>
    </if picture>

    <span class="date"><i>{strDatePublNews}</i></span>
    <p style="margin:12px 0 4px 0">{strNewsBody}</p>

    <p>« <a href="{cfgSiteUrl}news{stPage}">к списку новостей</a></p>
</div><!-- // cnt -->
</if novelty>
