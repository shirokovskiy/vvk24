<a name="results"></a>
<h3>Результаты поиска {strWhatSearched}</h3>
<div class="block">{blockPaginator}</div>

<div id="search_results" class="list">
	<loop list.results>
		<div class="row">
			<div class="cell col1"><a href="{cfgSiteUrl}{link}/{id}"><b>{strTitle}</b></a> <p>{strDescription}</p></div>
			<div class="cell clearer"><span class="salary">{strSalary}</span></div>
		</div>
	</loop list.results>
	<if no.results>
		<p>По заданным критериям {strCriterias}ничего не найдено!</p>

		<if cat.search>
			<p><a href="/po-otraslyam{strSearchAnotherCat}">выбрать другую отрасль</a></p>
		</if cat.search>
	</if no.results>
</div>

<div class="block">{blockPaginator}</div>
